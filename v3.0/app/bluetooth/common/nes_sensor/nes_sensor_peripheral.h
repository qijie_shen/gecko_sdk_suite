#ifndef NES_SENSOR_PERIPHERAL_H
#define NES_SENSOR_PERIPHERAL_H

#include "nes_sensor_config.h"

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
extern bool gpio_interruption_valid;
extern sl_simple_timer_t nes_sensor_gpio_interrupt_valid_check_timer;
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
extern sl_simple_timer_t nes_sensor_gpio_interrupt_wait_sync_single_timer;
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_PWM
extern bool pwm_is_running;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
extern uint32_t node_status;
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

void nes_sensor_peripheral_init(void);

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
void nes_sensor_gpio_detection_init(void);
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
void nes_sensor_gpio_detection_deinit(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
unsigned int nes_sensor_gpio_detection_read(void);

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
void nes_sensor_gpio_interrupt_init(void);
void nes_sensor_gpio_interrupt_deinit(void);
void nes_sensor_gpio_interrupt_enable(void);
void nes_sensor_gpio_interrupt_disable(void);
void nes_sensor_gpio_interrupt_cb(uint8_t pin);
void nes_sensor_gpio_interrupt_valid_check_timer_cb(sl_simple_timer_t *timer, void *data);
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
// Wait the transmitting sync timer callback.
void nes_sensor_gpio_interrupt_wait_sync_single_timer_cb(sl_simple_timer_t *timer, void *data);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_ADC
/**************************************************************************//**
 * Initialize NES Sensor PIR ADC0 peripheral.
 *****************************************************************************/
void nes_sensor_pir_adc0_init(void);

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
/**************************************************************************//**
 * DeInitialize NES Sensor PIR ADC0 peripheral.
 *****************************************************************************/
void nes_sensor_pir_adc0_deinit(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

/**************************************************************************//**
 * Read the sampling value of ADC0 peripheral.
 *****************************************************************************/
void nes_sensor_pir_adc0_read(float *adc_values);
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PWM
#if (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
void nes_sensor_pir_pwm0_init(void);
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
#if (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
void nes_sensor_pir_pwm0_deinit(void);
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
#if (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
void nes_sensor_pir_pwm1_init(void);
#endif // (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
#if (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
void nes_sensor_pir_pwm1_deinit(void);
#endif // (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
void turn_off_pwm_generation(void);
void turn_on_pwm_generation(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
void nes_sensor_pir_pyd1598_init(uint32_t reg_val);
int nes_sensor_pir_pyd1598_read(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
void nes_sensor_pir_si7021_temperature_init(void);
#if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE || NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
sl_status_t nes_sensor_pir_si7021_temperature_humidity_read(
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    float *temperature_read
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
    ,
#endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    float *humidity_read
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    );
#endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE || NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
void nes_sensor_pir_peripheral_init_extend_gpios(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS

#endif // NES_SENSOR_PERIPHERAL_H
