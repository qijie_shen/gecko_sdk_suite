
#ifndef NES_SENSOR_COMMUNICATION_H
#define NES_SENSOR_COMMUNICATION_H

#include <stdbool.h>
#include <stddef.h>
#include "nes_sensor_config.h"

#include "SensorData.pb-c.h"

extern NormalIndicationElement **element;
extern NESSensorData sensor_data;
extern SequenceStatus* sensor_sequence_status;
#if NES_PIR_MEASUREMENT_ENABLE_ADC
extern float adc_value_array[NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER][NES_PIR_MEASUREMENT_ADC_NUM];
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

void nes_sensor_pir_communication_init_sensor_data_structure(void);

/**************************************************************************//**
 * Convert PIR measurement value to characteristic buffer.
 * @param[in] value ADC sampling value.
 * @param[in] value ADC sampling value.
 * @param[in] gpio_value GPIO level value.
 * @param[in] pwm_enabled Value of PWM enable status (true or false).
 * @param[out] buffer Buffer to hold GATT characteristics value.
 *****************************************************************************/
void nes_sensor_pir_communication_measurement_val_to_buf(uint8_t index
#if NES_PIR_MEASUREMENT_ENABLE_ADC
                                       , float *adc_value
#elif NES_PIR_MEASUREMENT_ENABLE_PYD1598
                                       , int pyd1598_value
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC
#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
                                       , unsigned int gpio_value
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
                                       );

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
void nes_sensor_pir_communication_enable_gpio(void);
void nes_sensor_pir_communication_disable_gpio(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_ADC
void nes_sensor_pir_communication_enable_adc(void);
void nes_sensor_pir_communication_disable_adc(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PWM
void nes_sensor_pir_communication_enable_pwm(void);
void nes_sensor_pir_communication_disable_pwm(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
void nes_sensor_pir_communication_enable_si7021_temperature(void);
void nes_sensor_pir_communication_disable_si7021_temperature(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
void nes_sensor_pir_communication_enable_si7021_humidity(void);
void nes_sensor_pir_communication_disable_si7021_humidity(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
size_t nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(const uint8_t *buffer, size_t len);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))

#endif // NES_SENSOR_COMMUNICATION_H
