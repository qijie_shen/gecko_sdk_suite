/***************************************************************************//**
 * @file
 * @brief NES Sensor GATT service configuration
 *******************************************************************************
 * # License
 * <b>Copyright </b>
 *******************************************************************************
 *
 *
 ******************************************************************************/

#ifndef NES_SENSOR_CONFIG_H
#define NES_SENSOR_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> PIR Measurement Configuration

// <h> PIR Measurement Ability Configuration

// <q NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE> Enable PIR Work With Sleep Mode
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE                         1

#define NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE                           1

// <q NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION> Enable PIR GPIO Level Detection
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION                         1

// <q NES_PIR_MEASUREMENT_ENABLE_ADC> Enable PIR ADC Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_ADC                                          1

// <q NES_PIR_MEASUREMENT_ENABLE_PWM> Enable PIR PWM
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_PWM                                          1

// <q NES_PIR_MEASUREMENT_ENABLE_PYD1598> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_PYD1598                                      0

// <q NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION> Enable NES sensor code to handle network
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION                             1

// <q NES_PIR_MEASUREMENT_ENABLE_SI7021> Enable Si7021
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_SI7021                                       1

// <q NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS> Enable extend GPIO to drive other chips
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS                           0

// <q NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS> Enable status identification through specific GPIOs
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS                             1

// <q NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR> Enable the indication head char to help distinguish different msgs
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR                         1

// <q NES_PIR_MEASUREMENT_ENABLE_WATCHDOG> Enable the watch dog to avoid running off
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_WATCHDOG                                     1

// <q NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG> Enable the watch dog running in debug mode
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG                               0

// <q NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING> Enable smart advertising mode to save energy and help network recovery
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING                            1

// <q NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS> Enable the watch dog to avoid running off
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS          0

// <q NES_PIR_MEASUREMENT_LOG_ENABLE> Enable
// <i> Enables Log.
#define NES_PIR_MEASUREMENT_LOG_ENABLE                                          0

// <o NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE> Logging mode
// <NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_OFF=> Off
// <NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW=> Raw
// <NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG> Debug
// <NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF=> Protobuf
// <NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA>=> Protobuf pure data
#define NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE                                  NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_OFF

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_PWM> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_PWM                            0

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_GPIO> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_GPIO                           0

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC                            1

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_0> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_0                          0

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_1> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_1                          1

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_TEMP> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_TEMP                           0

// <q NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_HUMI> Enable PIR PYD1598 Sampling
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_HUMI                           0

// </h> End PIR Measurement Ability Configuration

// <h> PIR Measurement Working Status Configuration

// <o NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS>
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_1_SECOND=> 1_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND=> 2_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_4_SECOND=> 4_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_8_SECOND=> 8_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND=> 16_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_32_SECOND=> 32_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_64_SECOND=> 64_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_128_SECOND=> 128_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_256_SECOND=> 256_SECOND
// <i> Default: NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND
// <i> Define the timeout period for watchdog.
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS                     NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND

// <o NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE> Select the watchdog feed timer mode
// <NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SYSTEM_SOFT_TIMER=> System timer
// <NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER=> Simple timer
// <i> Default: NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SYSTEM_SOFT_TIMER
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE                            NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER

// <o NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS> <1-255>
// <i> Default: 1
// <i> Define the period to feed the watch dog, should smaller than the timeout period
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS                        8

// <o NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE> <0-255>
// <i> Default: 0
// <i> Define the timer handle to feed the watch dog
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE                          0

// <o NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE> Select the smart advertising timer mode
// <NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER=> System timer
// <NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER=> Simple timer
// <i> Default: NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE                        NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER

// <o NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS> <1-255>
// <i> Default: 20
// <i> Define the period to advertise
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS            20

// <o NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE> <0-255>
// <i> Default: 1
// <i> Define the timer handle to advertise
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE              1

// <o NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_PERIOD_SECONDS> <1-255>
// <i> Default: 40
// <i> Define the period to sleep before advertising
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_PERIOD_SECONDS           40

// <o NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_TIMER_HANDLE> <0-255>
// <i> Default: 2
// <i> Define the timer handle to sleep before advertising
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_TIMER_HANDLE             2

// <o NES_PIR_MEASUREMENT_ENTER_SLEEP_MODE_EXTERNAL_SIGNAL_ID> Sleep signal id
// <i> Default: 0x00000001
// <i> Define the sleep signal id.
#define NES_PIR_MEASUREMENT_ENTER_SLEEP_MODE_EXTERNAL_SIGNAL_ID                 0x00000001

// <o NES_PIR_MEASUREMENT_LOGGING_MODE> Indication timer mode
// <NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL=> Local logging
// <NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS=> Wireless logging
// <NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL=> Wireless with local logging
// <i> Default: NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL
// <i> Define the logging mode for each indication.
#define NES_PIR_MEASUREMENT_LOGGING_MODE                                        NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS

// <o NES_PIR_MEASUREMENT_ENERGY_MODE> Energy mode
// <NES_PIR_MEASUREMENT_ENERGY_MODE_EM1=> Energy Mode 1
// <NES_PIR_MEASUREMENT_ENERGY_MODE_EM2=> Energy Mode 2
// <NES_PIR_MEASUREMENT_ENERGY_MODE_EM3=> Energy Mode 3
// <i> Default: NES_PIR_MEASUREMENT_ENERGY_MODE_EM1
// <i> Define the run-time energy mode.
#define NES_PIR_MEASUREMENT_ENERGY_MODE                                         NES_PIR_MEASUREMENT_ENERGY_MODE_EM2

// <o NES_PIR_MEASUREMENT_TIMER_MODE> Indication timer mode
// <NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE=> Simple timer
// <NES_PIR_MEASUREMENT_TIMER_MODE_SLEEP=> Sleep timer
// <NES_PIR_MEASUREMENT_TIMER_MODE_LETIMER=> LETimer
// <NES_PIR_MEASUREMENT_TIMER_MODE_STD_TIMER=> Standard timer
// <NES_PIR_MEASUREMENT_TIMER_MODE_STD_WTIMER=> Standard wtimer
// <i> Default: NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE
// <i> Define the timer mode for each indication.
#define NES_PIR_MEASUREMENT_TIMER_MODE                                          NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE

// <o NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS> PIR Indication period (UUID: 2A21) <1-1000>
// <i> Default: 1000
// <i> Define the period of PIR data indication.
#define NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS                              50

// <o NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS> Wait interval before entering low power mode <1-1000>
// <i> Default: 10
// <i> Define the interval to wait before entering low power mode.
#define NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS        5

// <o NES_PIR_MEASUREMENT_WORK_INTERVAL_SECONDS> PIR measurement work period <10-60>
// <i> Default: 20
// <i> Define the period of PIR measurement work.
#define NES_PIR_MEASUREMENT_WORK_INTERVAL_SECONDS                               20

// <o NES_PIR_MEASUREMENT_SLEEP_INTERVAL_SECONDS> PIR measurement sleep period <10-300>
// <i> Default: 10
// <i> Define the period of PIR measurement sleep.
#define NES_PIR_MEASUREMENT_SLEEP_INTERVAL_SECONDS                              22

// <o NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER> Number of elements
// <i> Default: 20
// <i> Define the number of elements in one indication.
#define NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER                       10

// </h> End PIR Measurement Working Status Configuration

// <h> GPIO Level Detection Configuration

// <o NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_HIGH> GPIO high value (constant) <1-1>
// <i> Default: 1
// <i> Define the high value of GPIO level detection.
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_HIGH                               1

// <o NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_LOW> GPIO low value (constant) <0-0>
// <i> Default: 0
// <i> Define the Low value of GPIO level detection.
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_LOW                                0

// <q NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP> Enable GPIO Level Detection wake-up while sleeping
// <i> Default: 0
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP               1

// <q NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_INTERRUPT> Enable GPIO interrupt or not
// <i> Default: 1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_INTERRUPT            1

// <o NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_VALID_INTERVAL_SECONDS> Min interval to be valid
// <i> Default: 5
// <i> Define the period of GPIO level detection interrupt valid period.
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_VALID_INTERVAL_SECONDS   5

// <o NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE> Interrupt edge
// <i> Default: 0. 0=> rising_edge 1=>falling_edge
// <i> Define the period of GPIO level detection interrupt trigger.
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE             0

// <o NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_EXTERNAL_SIGNAL_ID> Interrupt callback external signal id
// <i> Default: 0x00000002
// <i> Define the sleep signal id.
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_EXTERNAL_SIGNAL_ID       0x00000002

// <q NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1> Use push button1 as the GPIO detection input for debugging
// <i> Default: 1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1                   0

// </h> End GPIO Level Detection Configuration

// <h> ADC Sampling Configuration

// <o NES_PIR_MEASUREMENT_ADC_SAMPLING_FREQUENCY> ADC sampling frequency
// <i> Default: 16000000
// <i> Define the frequency of ADC sampling.
#define NES_PIR_MEASUREMENT_ADC_SAMPLING_FREQUENCY                                  16000000

// <o NES_PIR_MEASUREMENT_ADC_NUM> ADC number <1-3>
// <i> Default: 1
// <i> Define the number of ADC.
#define NES_PIR_MEASUREMENT_ADC_NUM                                                 2

// <o NES_PIR_MEASUREMENT_ADC_RUNNING_MODE> ADC running mode
// <NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE=> Single mode
// <NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN=> Scan mode
// <i> Default: NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE
// <i> Define the ADC running mode.
#define NES_PIR_MEASUREMENT_ADC_RUNNING_MODE                                        NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN

// </h> End ADC Sampling Configuration

// <h> PWMs Configuration

// <o NES_PIR_MEASUREMENT_PWM0_TIM0_CC0_LOCATION> Location of TIM0_CC0 for PWM0
// <i> Default: TIMER_ROUTELOC0_CC0LOC_LOC18 - PD10
// <i> Define the location of TIM0_CC0 for PWM0.
#define NES_PIR_MEASUREMENT_PWM0_TIM0_CC0_LOCATION                                  TIMER_ROUTELOC0_CC0LOC_LOC18

// <o NES_PIR_MEASUREMENT_PWM1_TIM1_CC1_LOCATION> Location of TIM1_CC1 for PWM1
// <i> Default: TIMER_ROUTELOC0_CC1LOC_LOC18 - PD11
// <i> Define the location of TIM1_CC1 for PWM1.
#define NES_PIR_MEASUREMENT_PWM1_TIM1_CC1_LOCATION                                  TIMER_ROUTELOC0_CC1LOC_LOC18

// <o NES_PIR_MEASUREMENT_PWM_FREQUENCY> PWM output frequency in Hz <1-1000>
// <i> Default: 1000
// <i> Define the frequency of PWMs output.
#define NES_PIR_MEASUREMENT_PWM_FREQUENCY                                           1000

// <o NES_PIR_MEASUREMENT_PWM_DUTY_CYCLE> PWM duty cycle <1-100>
// <i> Default: 47
// <i> Define the duty cycle of PWMs output.
#define NES_PIR_MEASUREMENT_PWM_DUTY_CYCLE                                          47

// <o NES_PIR_MEASUREMENT_PWM1_PHASE_SHIFTING_COUNTER> PWM1 phase shifting counter
// <i> Default: 19000
// <i> Define the phase shifting counter of PWM1.
#define NES_PIR_MEASUREMENT_PWM1_PHASE_SHIFTING_COUNTER                             20000

// <o NES_PIR_MEASUREMENT_PWM_WORKING_SECONDS> PWM working seconds
// <i> Default: 4
// <i> 20 Elements in an indication => 4, 10 Elements in an indication => 8
// <i> Define the working seconds for each PWM running stage.
#define NES_PIR_MEASUREMENT_PWM_WORKING_SECONDS                                     8

// <q NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE> Enable PIR PWM separate on/off schedule
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE                     0

// <o NES_PIR_MEASUREMENT_PWM_ON_TIMES> PWM on times
// <i> Default: 8 for on 4 seconds
#define NES_PIR_MEASUREMENT_PWM_ON_TIMES                                            8

// <o NES_PIR_MEASUREMENT_PWM_OFF_TIMES> PWM off times
// <i> Default: 52 for off 26 seconds
#define NES_PIR_MEASUREMENT_PWM_OFF_TIMES                                           52

// <o NES_PIR_MEASUREMENT_GENERAL_WORKING_COUNTERS> PIR measurement general working counters <10-60>
// <i> Default: 20
// <i> Define the counters of PIR measurement general working.
#define NES_PIR_MEASUREMENT_GENERAL_WORKING_COUNTERS                                40

// <o NES_PIR_MEASUREMENT_PWM_WORKING_TIMES> PWM working times
// <i> Default: 5
// <i> Define the working times for working stage.
#define NES_PIR_MEASUREMENT_PWM_WORKING_TIMES                                       2

// <o NES_PIR_MEASUREMENT_PWM0_MODE> PWM0 mode
// <NES_PIR_MEASUREMENT_PWM0_MODE_NONE=> Disabled
// <NES_PIR_MEASUREMENT_PWM0_MODE_LETIMER=> LETimer
// <NES_PIR_MEASUREMENT_PWM0_MODE_TIMER=> Standard timer
// <NES_PIR_MEASUREMENT_PWM0_MODE_WTIMER=> Standard wtimer
// <i> Default: NES_PIR_MEASUREMENT_PWM0_MODE_TIMER
// <i> Define the timer mode for PWM0.
#define NES_PIR_MEASUREMENT_PWM0_MODE                                               NES_PIR_MEASUREMENT_PWM0_MODE_TIMER

// <o NES_PIR_MEASUREMENT_PWM1_MODE> PWM1 mode
// <NES_PIR_MEASUREMENT_PWM1_MODE_NONE=> Disabled
// <NES_PIR_MEASUREMENT_PWM1_MODE_LETIMER=> LETimer
// <NES_PIR_MEASUREMENT_PWM1_MODE_TIMER=> Standard timer
// <NES_PIR_MEASUREMENT_PWM1_MODE_WTIMER=> Standard wtimer
// <i> Default: NES_PIR_MEASUREMENT_PWM1_MODE_TIMER
// <i> Define the timer mode for PWM1.
#define NES_PIR_MEASUREMENT_PWM1_MODE                                               NES_PIR_MEASUREMENT_PWM1_MODE_TIMER

// </h> End PWMs Configuration

// <h> PYD1598 Configuration

// <q NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO> High efficiency GPIO
// <i> Default: 1
#define NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO                     1

// <o NES_PIR_MEASUREMENT_PYD1598_BAND_PASS_INIT> Band pass init value (constant) <0x00000010-0x00000010>
// <i> Default: 0x00000010
// <i> Define the band pass init value of PYD1598.
#define NES_PIR_MEASUREMENT_PYD1598_BAND_PASS_INIT                                  0x00000010

// <o NES_PIR_MEASUREMENT_PYD1598_LOW_PASS_INIT> Low pass init value (constant) <0x00000030-0x00000030>
// <i> Default: 0x00000030
// <i> Define the low pass init value of PYD1598.
#define NES_PIR_MEASUREMENT_PYD1598_LOW_PASS_INIT                                   0x00000030

// </h> End PYD1598 Configuration

// <h> Si7021 Temperature Configuration

// <q NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE> Enable Si7021 to measure temperature
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE                               1

// <q NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY> Enable Si7021 to measure humidity
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY                                  1

// </h> End Si7021 Temperature Configuration

// <h> Register Configuration

// <o NES_PIR_MEASUREMENT_REGISTER_PERIODICAL_TIMER_INTERVAL_SECONDS> Min interval to be valid
// <i> Default: 5
// <i> Define the period of GPIO level detection interrupt valid period.
#define NES_PIR_MEASUREMENT_REGISTER_PERIODICAL_TIMER_INTERVAL_SECONDS   						120

// </h> End of Register Configuration

// </h> End PIR Measurement Configuration

// <<< end of configuration section >>>

// <<< sl:start pin_tool >>>

// Node status definition GPIOs
// <gpio> NES_PIR_MEASUREMENT_STATUS_DEFINITION_0 - P0 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_STATUS_DEFINITION_0]
#define NES_PIR_MEASUREMENT_STATUS_DEFINITION_0_PORT                gpioPortA
#define NES_PIR_MEASUREMENT_STATUS_DEFINITION_0_PIN                 2
// [GPIO_NES_PIR_MEASUREMENT_STATUS_DEFINITION_0]$

// <gpio> NES_PIR_MEASUREMENT_STATUS_DEFINITION_1 - P2 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_STATUS_DEFINITION_1]
#define NES_PIR_MEASUREMENT_STATUS_DEFINITION_1_PORT                gpioPortA
#define NES_PIR_MEASUREMENT_STATUS_DEFINITION_1_PIN                 3
// [GPIO_NES_PIR_MEASUREMENT_STATUS_DEFINITION_1]$

// <gpio> NES_PIR_MEASUREMENT_STATUS_DEFINITION_2 - P3 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_STATUS_DEFINITION_2]
#define NES_PIR_MEASUREMENT_STATUS_DEFINITION_2_PORT                gpioPortC
#define NES_PIR_MEASUREMENT_STATUS_DEFINITION_2_PIN                 7
// [GPIO_NES_PIR_MEASUREMENT_STATUS_DEFINITION_2]$

// ADC GPIOs
// <gpio> NES_PIR_MEASUREMENT_ADC_CHANNEL0 - P7 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_ADC_CHANNEL0]
#define NES_PIR_MEASUREMENT_ADC_CHANNEL0_PORT                       gpioPortC
#define NES_PIR_MEASUREMENT_ADC_CHANNEL0_PIN                        9
// [GPIO_NES_PIR_MEASUREMENT_ADC_CHANNEL0]$

// <gpio> NES_PIR_MEASUREMENT_ADC_CHANNEL1 - P30 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_ADC_CHANNEL1]
#define NES_PIR_MEASUREMENT_ADC_CHANNEL1_PORT                       gpioPortF
#define NES_PIR_MEASUREMENT_ADC_CHANNEL1_PIN                        4
// [GPIO_NES_PIR_MEASUREMENT_ADC_CHANNEL1]$

// <gpio> NES_PIR_MEASUREMENT_ADC_CHANNEL2 - P32 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_ADC_CHANNEL2]
#define NES_PIR_MEASUREMENT_ADC_CHANNEL2_PORT                       gpioPortF
#define NES_PIR_MEASUREMENT_ADC_CHANNEL2_PIN                        5
// [GPIO_NES_PIR_MEASUREMENT_ADC_CHANNEL2]$

// GPIO Detection GPIOs
// <gpio> NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION - P1 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION]
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT               gpioPortF
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN                7
#else // !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT               gpioPortC
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN                6
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
// [GPIO_NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION]$

// PYD1598 GPIOs
// <gpio> NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN - P31 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN]
// #define NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PORT               gpioPortD
// #define NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PIN                13
// [GPIO_NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN]$

// <gpio> NES_PIR_MEASUREMENT_PYD1598_DL - P33 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_PYD1598_DL]
// #define NES_PIR_MEASUREMENT_PYD1598_DL_PORT                      gpioPortD
// #define NES_PIR_MEASUREMENT_PYD1598_DL_PIN                       14
// [GPIO_NES_PIR_MEASUREMENT_PYD1598_DL]$

// PWM GPIOs
// <gpio> NES_PIR_MEASUREMENT_PWM0 (TIM0_CC0 #18) - P4 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_PWM0]
#define NES_PIR_MEASUREMENT_PWM0_PORT                               gpioPortD
#define NES_PIR_MEASUREMENT_PWM0_PIN                                10
// [GPIO_NES_PIR_MEASUREMENT_PWM0]$

// <gpio> NES_PIR_MEASUREMENT_PWM1 (TIM1_CC1 #18) - P6 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_PWM1]
#define NES_PIR_MEASUREMENT_PWM1_PORT                               gpioPortD
#define NES_PIR_MEASUREMENT_PWM1_PIN                                11
// [GPIO_NES_PIR_MEASUREMENT_PWM1]$

// Extend GPIOs
// <gpio> NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0 - P18 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0]
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PORT            gpioPortB
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PIN             11
// [GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0]$

// <gpio> NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1 - P20 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1]
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PORT            gpioPortB
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PIN             12
// [GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1]$

// <gpio> NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2 - P22 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2]
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PORT            gpioPortB
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PIN             13
// [GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2]$

// <gpio> NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0 - P8 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0]
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PORT      gpioPortD
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PIN       12
// [GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0]$

// <gpio> NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1 - P31 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1]
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PORT      gpioPortD
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PIN       13
// [GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1]$

// <gpio> NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2 - P33 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2]
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PORT      gpioPortD
#define NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PIN       14
// [GPIO_NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2]$

// <<< sl:end pin_tool >>>

#endif // NES_SENSOR_CONFIG_H
