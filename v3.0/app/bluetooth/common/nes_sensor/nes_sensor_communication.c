#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "nes_sensor.h"

#include "SensorData.pb-c.h"

NormalIndicationElement **element;
NESSensorData sensor_data;
SequenceStatus* sensor_sequence_status;
#if NES_PIR_MEASUREMENT_ENABLE_ADC
float adc_value_array[NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER][NES_PIR_MEASUREMENT_ADC_NUM];
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

void nes_sensor_pir_communication_init_sensor_data_structure(void)
{
  uint8_t i;

  element = NULL;

  element = (NormalIndicationElement**)malloc(sizeof(NormalIndicationElement*) * NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER);
  if (element) {
    for (i=0; i<NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER; i++) {
      element[i] = (NormalIndicationElement*)malloc(sizeof(NormalIndicationElement));
      normal__indication__element__init(element[i]);
#if NES_PIR_MEASUREMENT_ENABLE_ADC
      element[i]->n_adc_values = NES_PIR_MEASUREMENT_ADC_NUM;
      element[i]->adc_values = adc_value_array[i];
#elif NES_PIR_MEASUREMENT_ENABLE_PYD1598

#endif // NES_PIR_MEASUREMENT_ENABLE_ADC
#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
      element[i]->has_gpio = true;
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
    }
  }

  // Indication stage = start as default
  nes__sensor_data__init(&sensor_data);
  //sensor_data.sequence_status = sensor_sequence_status;
#if NES_PIR_MEASUREMENT_ENABLE_PWM
  sensor_data.has_pwm_status = true;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
  sensor_data.has_temp = true;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  sensor_data.has_humidity = true;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  sensor_data.n_element = NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER;
  if (element) {
    sensor_data.element = element;
  }
}

void nes_sensor_pir_communication_measurement_val_to_buf(uint8_t index
#if NES_PIR_MEASUREMENT_ENABLE_ADC
                                       , float *adc_value
#elif NES_PIR_MEASUREMENT_ENABLE_PYD1598
                                       , int pyd1598_value
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC
#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
                                       , unsigned int gpio_value
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
                                       )
{
#if NES_PIR_MEASUREMENT_ENABLE_ADC
  uint8_t i;
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

  // ADC
#if NES_PIR_MEASUREMENT_ENABLE_ADC
  for (i=0; i<NES_PIR_MEASUREMENT_ADC_NUM; i++) {
    adc_value_array[index][i] = adc_value[i];
  }
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

  // GPIO
#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
  element[index]->gpio = gpio_value;
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
}

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
void nes_sensor_pir_communication_enable_gpio(void)
{
  uint8_t i;
  if (element) {
    for (i=0; i<NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER; i++) {
      if (element[i]) {
        element[i]->has_gpio = true;
      }
    }
  }
}

void nes_sensor_pir_communication_disable_gpio(void)
{
  uint8_t i;
  if (element) {
    for (i=0; i<NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER; i++) {
      if (element[i]) {
        element[i]->has_gpio = false;
      }
    }
  }
}
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_ADC
void nes_sensor_pir_communication_enable_adc(void)
{

}

void nes_sensor_pir_communication_disable_adc(void)
{

}
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PWM
void nes_sensor_pir_communication_enable_pwm(void)
{
  sensor_data.has_pwm_status = true;
}

void nes_sensor_pir_communication_disable_pwm(void)
{
  sensor_data.has_pwm_status = false;
}
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
void nes_sensor_pir_communication_enable_si7021_temperature(void)
{
  sensor_data.has_temp = true;
}

void nes_sensor_pir_communication_disable_si7021_temperature(void)
{
  sensor_data.has_temp = false;
}
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
void nes_sensor_pir_communication_enable_si7021_humidity(void)
{
  sensor_data.has_humidity = true;
}

void nes_sensor_pir_communication_disable_si7021_humidity(void)
{
  sensor_data.has_humidity = false;
}
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
size_t nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(const uint8_t *buffer, size_t len)
{
  NESSensorData *sensor_data = NULL;
  uint8_t i, j;

#if NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
  len--;
  sensor_data = nes__sensor_data__unpack(NULL, len, &buffer[1]);
#else // !NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
  sensor_data = nes__sensor_data__unpack(NULL, len, buffer);
#endif // NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
  if(!sensor_data) {
      nes_sensor_server_log("nes__sensor_data__unpack is fail!!!\n");
    return 1;
  }
#if 0
  if (sensor_data->sequence_status->packet_loss) {
    nes_sensor_server_log("Indication has packets lost. ");
    nes_sensor_server_log("Blocked sequence is %lu and current sequence is %lu.\n",
               sensor_data->sequence_status->blocked_sequence,
               sensor_data->sequence_status->current_sequence);
  } else {
    nes_sensor_server_log("Indication has no packets lost. ");
    nes_sensor_server_log("Current sequence is %lu.\n",
               sensor_data->sequence_status->current_sequence);
  }
#endif

#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF)
  switch(sensor_data->type) {
    case INDICATION_TYPE__Normal_Start:
      nes_sensor_server_log("Normal start ");
      break;
    case INDICATION_TYPE__Normal_Unconcerned:
      nes_sensor_server_log("Normal unconcerned ");
      break;
    case INDICATION_TYPE__Normal_End:
      nes_sensor_server_log("Normal end ");
      break;
    default:
      break;
  }
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF)

  switch(sensor_data->type) {
    case INDICATION_TYPE__Normal_Start:
    case INDICATION_TYPE__Normal_End:
    case INDICATION_TYPE__Normal_Unconcerned:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF)
      //nes_sensor_server_log("Normal ");

      if (sensor_data->has_pwm_status) {
          nes_sensor_server_log("PWM is on: ");
        if (sensor_data->pwm_status) {
          nes_sensor_server_log("1,");
        } else {
          nes_sensor_server_log("0,");
        }
      } else {
        nes_sensor_server_log("PWM is off\n");
      }

      // temp
      if (sensor_data->has_temp) {
        nes_sensor_server_log("temp is on: ");
        nes_sensor_server_log("%f,", sensor_data->temp);
      } else {
        nes_sensor_server_log("temp is off| ");
      }

      // humidity
      if (sensor_data->has_humidity) {
        nes_sensor_server_log("humidity is on: ");
        nes_sensor_server_log("%f,", sensor_data->humidity);
      } else {
        nes_sensor_server_log("humidity is off| ");
      }

      nes_sensor_server_log("\n");

      if (sensor_data->n_element) {
        for (i = 0; i< sensor_data->n_element; i++) {
          // ADC
          if (sensor_data->element[i]->n_adc_values) {
            nes_sensor_server_log("ADC is on: ");

            for (j=0; j<sensor_data->element[i]->n_adc_values; j++) {
              nes_sensor_server_log("ADC[%d] = ", j);
              nes_sensor_server_log("%f,", sensor_data->element[i]->adc_values[j]);
            }
            nes_sensor_server_log(" ");
          } else {
            nes_sensor_server_log("ADC is off| ");
          }

          // gpio
          if (sensor_data->element[i]->has_gpio) {
            nes_sensor_server_log("GPIO is on: ");

            nes_sensor_server_log("GPIO is %d\n", (sensor_data->element[i]->gpio == true) ? (1):(0));
          } else {
            nes_sensor_server_log("gpio is off\n");
          }
        }
      }
#else // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA)
      if (sensor_data->n_element) {
        for (i = 0; i< sensor_data->n_element; i++) {
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_PWM
          if (sensor_data->has_pwm_status) {
            if (sensor_data->pwm_status) {
              nes_sensor_server_log("1,");
            } else {
              nes_sensor_server_log("0,");
            }
          }
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_PWM
#if NES_PIR_MEASUREMENT_ENABLE_SI7021
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_TEMP
          // temp
          if (sensor_data->has_temp) {
            nes_sensor_server_log("%f,", sensor_data->temp);
          }
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_TEMP
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_HUMI
          // humidity
          if (sensor_data->has_humidity) {
            nes_sensor_server_log("%f,", sensor_data->humidity);
          }
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_HUMI
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC
          // ADC
          if (sensor_data->element[i]->n_adc_values) {
            for (j=0; j<sensor_data->element[i]->n_adc_values; j++) {
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_0
              if (j == 0) {
                nes_sensor_server_log("%f,", sensor_data->element[i]->adc_values[j]);
              }
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_0
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_1
              if (j == 1) {
                nes_sensor_server_log("%f", sensor_data->element[i]->adc_values[j]);
              }
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC_1
            }
          }
          nes_sensor_server_log("\n");
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_ADC
#if NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_GPIO
          // gpio
          if (sensor_data->element[i]->has_gpio) {
            nes_sensor_server_log("%d", (sensor_data->element[i]->gpio == true) ? (1):(0));
          }
#endif // NES_PIR_MEASUREMENT_ENABLE_LOCAL_LOGGING_GPIO
          //nes_sensor_server_log("\n");
        }
      }
#endif //
      break;
    case INDICATION_TYPE__Interrupt:
      nes_sensor_server_log("Interrupt\n");
      break;
    default:
      break;
  }

#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF)
  nes_sensor_server_log("\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF)
  nes__sensor_data__free_unpacked(sensor_data, NULL);

  return 0;
}
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
