/***************************************************************************//**
 * @file
 * @brief NES Sensor GATT service
 *******************************************************************************
 * # License
 * <b>Copyright </b>
 *******************************************************************************
 *
 *
 ******************************************************************************/

#include <stdbool.h>
#include "em_common.h"
#include "sl_bluetooth.h"
#include "gatt_db.h"
#include "sl_app_assert.h"
#include "nes_sensor.h"

#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT
#ifdef SL_CATALOG_CLI_PRESENT
#include "sl_cli.h"
#endif // SL_CATALOG_CLI_PRESENT
#if defined(SL_CATALOG_POWER_MANAGER_PRESENT)
#include "sl_power_manager.h"
#endif // SL_CATALOG_POWER_MANAGER_PRESENT

/// Header files
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#include "sl_simple_timer.h"
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#include "em_wdog.h"
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

#include "nes_sensor_peripheral.h"
#include "nes_sensor_communication.h"

/// Macro definitions

/// Static variable definitions
static running_mode_t running_mode;
static uint32_t collecting_sequence;
static uint32_t power_detection_sequence;
static uint32_t testing_sequence;
static packet_status_t packet_status;

static bool if_ok_to_indicate = false;

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
extern uint32_t node_status;
static sl_simple_timer_t nes_pir_measurement_periodic_register_timer;
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
// Connection handle.
static uint8_t app_connection = 0;
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
extern uint8_t advertising_set_handle;
#endif // NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
// Periodic PIR indication timer handle.
static sl_simple_timer_t nes_pir_measurement_periodic_timer;
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

// Store sampling data into a buffer to indicate.
uint8_t nes_pir_measurement_indication_buffer[NES_PIR_MEASUREMENT_INDICATION_BUFFER_LENGTH_BYTES];
size_t buffer_length;

#if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
bool is_in_low_power_mode;
#endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
static sl_simple_timer_t nes_pir_measurement_sleep_wakeup_timer;
static sl_simple_timer_t nes_pir_measurement_wait_sync_single_timer;
static uint8_t open_close_connection = 0;
static bool is_in_working_mode;
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
static sl_simple_timer_t nes_pir_measurement_smart_advertising_working_timer;
static sl_simple_timer_t nes_pir_measurement_smart_advertising_sleeping_timer;
#endif // (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
static const WDOG_Init_TypeDef wdg_init =
{
  .enable = true,             /* Start watchdog when init done */
#if !NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG
  .debugRun = false,          /* WDOG not counting during debug halt */
#else // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG
  .debugRun = true,           /* WDOG not counting during debug halt */
#endif // !NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG
  .em2Run = true,             /* WDOG counting when in EM2 */
  .em3Run = true,             /* WDOG counting when in EM3 */
  .em4Block = false,          /* EM4 can be entered */
  .swoscBlock = false,        /* Do not block disabling LFRCO/LFXO in CMU */
  .lock = false,              /* Do not lock WDOG configuration (if locked, reset needed to unlock) */
  .clkSel = wdogClkSelULFRCO, /* Select 1kHZ WDOG oscillator */
#if (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_1_SECOND)
  .perSel = wdogPeriod_1k,    /* Set the watchdog period to 1025 clock periods (ie ~1 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND)
  .perSel = wdogPeriod_2k,    /* Set the watchdog period to 2049 clock periods (ie ~2 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_4_SECOND)
  .perSel = wdogPeriod_4k,    /* Set the watchdog period to 4097 clock periods (ie ~4 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_8_SECOND)
  .perSel = wdogPeriod_8k,    /* Set the watchdog period to 8193 clock periods (ie ~8 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND)
  .perSel = wdogPeriod_16k,    /* Set the watchdog period to 16385 clock periods (ie ~16 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_32_SECOND)
  .perSel = wdogPeriod_32k,    /* Set the watchdog period to 32769 clock periods (ie ~32 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_64_SECOND)
  .perSel = wdogPeriod_64k,    /* Set the watchdog period to 65537 clock periods (ie ~64 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_128_SECOND)
  .perSel = wdogPeriod_128k,    /* Set the watchdog period to 131073 clock periods (ie ~128 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_256_SECOND)
  .perSel = wdogPeriod_256k,    /* Set the watchdog period to 262145 clock periods (ie ~256 seconds)*/
#endif
};

#if (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
static sl_simple_timer_t nes_pir_measurement_watchdog_feed_timer;
#endif // (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

// Periodical timeout handles
// Used to check if it's the time to indicate.
static uint8_t counter = 0;
#if NES_PIR_MEASUREMENT_ENABLE_PWM
static uint8_t pwm_pulse_counter = 0;
static uint8_t pwm_running_counter = 0;
#if NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
pwm_running_status_t pwm_running_status;
#endif // NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if !NES_PIR_MEASUREMENT_ENABLE_PWM
static uint8_t working_time_counter = 0;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

/// Static function declarations
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
// Periodic PIR indication timer callback.
static void nes_pir_measurement_periodic_timer_cb(sl_simple_timer_t *timer, void *data);
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
static void nes_pir_measurement_general_deinit(void);
static void nes_pir_measurement_sleep_wakeup_timer_cb(sl_simple_timer_t *timer, void *data);
// Wait the transmitting sync timer callback.
static void nes_pir_measurement_wait_sync_single_timer_cb(sl_simple_timer_t *timer, void *data);
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
static void nes_pir_measurement_smart_advertising_enter(void);
#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
static void nes_pir_measurement_smart_advertising_working_timer_cb(sl_simple_timer_t *timer, void *data);
static void nes_pir_measurement_smart_advertising_sleeping_timer_cb(sl_simple_timer_t *timer, void *data);
#endif // (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#if (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
static void nes_pir_measurement_watchdog_feed_timer_cb(sl_simple_timer_t *timer, void *data);
#endif // (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
static void nes_pir_measurement_periodic_register_timer_cb(sl_simple_timer_t *timer, void *data);
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
/**************************************************************************//**
 * Initialize NES Sensor PIR module.
 *****************************************************************************/
static void nes_sensor_pir_init(void);

/**************************************************************************//**
 * Initialize NES Sensor module.
 *****************************************************************************/
static void nes_sensor_init(void);

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
/**************************************************************************//**
 * Callback function of connection close event.
 *
 * @param[in] reason Unused parameter required by the nes_sensor component
 * @param[in] connection Unused parameter required by the nes_sensor component
 *****************************************************************************/
static void nes_bt_sensor_measurement_connection_closed_cb(uint16_t reason, uint8_t connection);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
/**************************************************************************//**
 * NES Sensor - Sensor Measurement
 * Indication changed callback
 *
 * Called when indication of nes sesnor measurement is enabled/disabled by
 * the client.
 *****************************************************************************/
SL_WEAK void nes_bt_sensor_measurement_indication_changed_cb(uint8_t connection,
                                                                    gatt_client_config_flag_t client_config)
{
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

#if !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc_timer;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS

  app_connection = connection;
  if_ok_to_indicate = true;

#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  nes_sensor_server_log("The indication ability is changed\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  nes_sensor_pir_init();

  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    // Start timer used for periodic indications.
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_simple_timer_start(&nes_pir_measurement_periodic_timer,
                               NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS,
                               nes_pir_measurement_periodic_timer_cb,
                               NULL,
                               true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start periodic timer\n",
                  (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
  }
  // Indications disabled.
  else {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
    nes_sensor_server_log("nes_bt_sensor_measurement_indication_changed_cb(work_with_sleep_mode|multi-chars): client_config = gatt_disable.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  if (is_in_working_mode) {
    nes_sensor_pir_init();

    // Indication or notification enabled.
    if (gatt_disable != client_config) {
#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
      // Indicate node status
      nes_pir_measurement_pack_sequence_and_indicate(indication_mode_register);
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

      // Start timer used for periodic indications.
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_simple_timer_start(&nes_pir_measurement_periodic_timer,
                                 NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS,
                                 nes_pir_measurement_periodic_timer_cb,
                                 NULL,
                                 true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start periodic timer\n",
                    (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
    }
    // Indications disabled.
    else {
      //nes_pir_measurement_bt_deinit
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      nes_sensor_server_log("nes_bt_sensor_measurement_indication_changed_cb(is_in_working_mode|single char): client_config = gatt_disable.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_DEBUG)
    }
  }
#if !NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
  else {
#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && \
  NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)

  // Send PIR measurement indication to connected client.
  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    nes_pir_measurement_pack_sequence_and_indicate(indication_mode_pir_sleep_measurement);
  } else {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
    nes_sensor_server_log("nes_bt_sensor_measurement_indication_changed_cb(is_in_idle_mode|single char): client_config = gatt_disable.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  }
  // Print the message.
#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL)
  nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(nes_pir_measurement_indication_buffer, buffer_length);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc_timer =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_sensor_gpio_interrupt_wait_sync_single_timer,
                         NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS,
                         nes_sensor_gpio_interrupt_wait_sync_single_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc_timer == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start wait-sync-single timer\n",
                (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION &&
      // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
  }
#endif // NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
#else // !NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
  nes_sensor_pir_init();

  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    // Start timer used for periodic indications.
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_simple_timer_start(&nes_pir_measurement_periodic_timer,
                               NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS,
                               nes_pir_measurement_periodic_timer_cb,
                               NULL,
                               true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start periodic timer\n",
                  (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
  }
  // Indications disabled.
  else {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
    nes_sensor_server_log("nes_bt_sensor_measurement_indication_changed_cb(is_not_in_work_with_sleep_mode): client_config = gatt_disable.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  }
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
}

#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
/**************************************************************************//**
 * NES Sensor - Sensor Sleep Measurement
 * Indication changed callback
 *
 * Called when indication of nes sesnor measurement is enabled/disabled by
 * the client.
 *****************************************************************************/
SL_WEAK void nes_bt_sensor_sleep_measurement_indication_changed_cb(uint8_t connection,
                                                                    gatt_client_config_flag_t client_config)
{
#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc_timer;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)

  app_connection = connection;

#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && \
  NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP && \
  NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE)

  // Send PIR measurement indication to connected client.
  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    nes_pir_measurement_pack_sequence_and_indicate(indication_mode_pir_sleep_measurement);
  } else {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
    nes_sensor_server_log("nes_bt_sensor_sleep_measurement_indication_changed_cb: client_config = gatt_disable.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  }

  // Print the message.
#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL)
  nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(nes_pir_measurement_indication_buffer, buffer_length);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc_timer =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_sensor_gpio_interrupt_wait_sync_single_timer,
                         NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS,
                         nes_sensor_gpio_interrupt_wait_sync_single_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc_timer == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start wait-sync-single timer\n",
                (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION &&
      // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP &&
      // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE)
}
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS

/**************************************************************************//**
 * PIR Measurement characteristic indication confirmed.
 *****************************************************************************/
SL_WEAK void nes_bt_sensor_measurement_indication_confirmed_cb(uint8_t connection)
{
  (void)connection;

  // Use a timer to
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  nes_sensor_server_log("The indication confirmation is received\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
}
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

/**************************************************************************//**
 * Bluetooth stack event handler.
 *****************************************************************************/
void nes_bt_sensor_measurement_on_event(sl_bt_msg_t *evt)
{
  // Handle stack events
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_bt_evt_system_boot_id:
      nes_sensor_init();
      break;

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
    case sl_bt_evt_connection_opened_id:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      nes_sensor_server_log("The connection is open\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
      nes_sensor_server_log("Stop the advertising totally\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
      // Stop the advertising
      sl_bt_advertiser_stop(advertising_set_handle);
      // Stop advertising related timers
#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
      sl_bt_system_set_soft_timer(0,
                                  NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE,
                                  0);
      sl_bt_system_set_soft_timer(0,
                                  NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_TIMER_HANDLE,
                                  0);
#elif (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
      sl_simple_timer_stop(&nes_pir_measurement_smart_advertising_working_timer);
      sl_simple_timer_stop(&nes_pir_measurement_smart_advertising_sleeping_timer);
#endif //
#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      open_close_connection = evt->data.evt_connection_opened.connection;
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      break;

    case sl_bt_evt_connection_closed_id:
      nes_bt_sensor_measurement_connection_closed_cb(evt->data.evt_connection_closed.reason,
                                 evt->data.evt_connection_closed.connection);
      break;

    case sl_bt_evt_gatt_server_characteristic_status_id:
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
      if (is_in_working_mode) {
        if (gattdb_pir_measurement == evt->data.evt_gatt_server_characteristic_status.characteristic) {
          // client characteristic configuration changed by remote GATT client
          if (gatt_server_client_config == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            nes_bt_sensor_measurement_indication_changed_cb(
              evt->data.evt_gatt_server_characteristic_status.connection,
              evt->data.evt_gatt_server_characteristic_status.client_config_flags);
          }
          // confirmation of indication received from remove GATT client
          else if (gatt_server_confirmation == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            nes_bt_sensor_measurement_indication_confirmed_cb(
              evt->data.evt_gatt_server_characteristic_status.connection);
          } else {
            sl_app_assert(false,
                          "[E: 0x%04x] Unexpected status flag in evt_gatt_server_characteristic_status\n",
                          (int)evt->data.evt_gatt_server_characteristic_status.status_flags);
          }
        }
      } else {
        if (gattdb_pir_sleep_measurement == evt->data.evt_gatt_server_characteristic_status.characteristic) {
          // client characteristic configuration changed by remote GATT client
          if (gatt_server_client_config == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            nes_bt_sensor_sleep_measurement_indication_changed_cb(
              evt->data.evt_gatt_server_characteristic_status.connection,
              evt->data.evt_gatt_server_characteristic_status.client_config_flags);
          }
          // confirmation of indication received from remove GATT client
          else if (gatt_server_confirmation == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            nes_bt_sensor_measurement_indication_confirmed_cb(
              evt->data.evt_gatt_server_characteristic_status.connection);
          } else {
            sl_app_assert(false,
                          "[E: 0x%04x] Unexpected status flag in evt_gatt_server_characteristic_status\n",
                          (int)evt->data.evt_gatt_server_characteristic_status.status_flags);
          }
        }
      }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
      if (gattdb_pir_measurement == evt->data.evt_gatt_server_characteristic_status.characteristic) {
        // client characteristic configuration changed by remote GATT client
        if (gatt_server_client_config == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
          nes_bt_sensor_measurement_indication_changed_cb(
            evt->data.evt_gatt_server_characteristic_status.connection,
            evt->data.evt_gatt_server_characteristic_status.client_config_flags);
        }
        // confirmation of indication received from remove GATT client
        else if (gatt_server_confirmation == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
          nes_bt_sensor_measurement_indication_confirmed_cb(
            evt->data.evt_gatt_server_characteristic_status.connection);
        } else {
          sl_app_assert(false,
                        "[E: 0x%04x] Unexpected status flag in evt_gatt_server_characteristic_status\n",
                        (int)evt->data.evt_gatt_server_characteristic_status.status_flags);
        }
      }
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
      break;
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

    case sl_bt_evt_system_external_signal_id:
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      if (NES_PIR_MEASUREMENT_ENTER_SLEEP_MODE_EXTERNAL_SIGNAL_ID == evt->data.evt_system_external_signal.extsignals) {

      }
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      break;

    case sl_bt_evt_system_soft_timer_id:
      switch (evt->data.evt_system_soft_timer.handle) {
#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#if (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
        case NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE:
          WDOG_Feed();
          break;
#endif // (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
        case NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
          nes_sensor_server_log("Working advertising is timeout.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
          // Stop advertising
          sl_bt_advertiser_stop(advertising_set_handle);
          // Start the advertising sleeping timer
          sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_PERIOD_SECONDS),
                                      NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_TIMER_HANDLE,
                                      1);
          break;
        case NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_TIMER_HANDLE:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
          nes_sensor_server_log("Sleeping advertising is timeout.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
          // Start advertising
          sl_bt_advertiser_start(
            advertising_set_handle,
            advertiser_general_discoverable,
            advertiser_connectable_scannable);
          // Start the advertising working timer
          sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS),
                                      NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE,
                                      1);
          break;
#endif // (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
        default:
          break;
      }
      break;

    default:
      break;
  }
}

// -----------------------------------------------------------------------------
// Private function definitions

#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
/**************************************************************************//**
 * Timer callback
 * Called periodically to time periodic temperature measurements and indications.
 *****************************************************************************/
static void nes_pir_measurement_periodic_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc_timer;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if NES_PIR_MEASUREMENT_ENABLE_ADC
  float adc_value[NES_PIR_MEASUREMENT_ADC_NUM];
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
  int pyd1598_value = 0;
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
  unsigned int gpio_value = 0;
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
  float temperature;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  float humidity;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

  /// Get sensors' values.
#if NES_PIR_MEASUREMENT_ENABLE_ADC
  nes_sensor_pir_adc0_read(adc_value);
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
  gpio_value = nes_sensor_gpio_detection_read();
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
  pyd1598_value = nes_sensor_pir_pyd1598_read();
  pyd1598_value = (pyd1598_value * 330) / 8192;
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
  temperature = 0.0;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  humidity = 0.0;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY

  // Only the first packet will collect temperature data each period
  if (sensor_data.type == INDICATION_TYPE__Normal_Start) {
    nes_sensor_pir_si7021_temperature_humidity_read(
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
        &temperature
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
        ,
#endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
        &humidity
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
        );

#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    if (sensor_data.has_temp) {
      sensor_data.temp = temperature;
    }
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    if (sensor_data.has_humidity) {
      sensor_data.humidity = humidity;
    }
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  }
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

  nes_sensor_pir_communication_measurement_val_to_buf(counter
#if NES_PIR_MEASUREMENT_ENABLE_ADC
                             , adc_value
#elif NES_PIR_MEASUREMENT_ENABLE_PYD1598
                             , pyd1598_value
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC
#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
                             , gpio_value
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
                             );

  counter++;

  if(counter == NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER) {
#if !NES_PIR_MEASUREMENT_ENABLE_PWM
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
    // Send PIR measurement indication to connected client.
    nes_pir_measurement_pack_sequence_and_indicate(indication_mode_pir_measurement);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
    // Print the message.
#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
    nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(nes_pir_measurement_indication_buffer, buffer_length);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))

#endif // !NES_PIR_MEASUREMENT_ENABLE_PWM

    counter = 0;

#if NES_PIR_MEASUREMENT_ENABLE_PWM
    pwm_pulse_counter++;
    // Send PIR measurement indication to connected client.
    nes_pir_measurement_pack_sequence_and_indicate(indication_mode_pir_measurement);
#if !NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
    if (pwm_pulse_counter == NES_PIR_MEASUREMENT_PWM_WORKING_SECONDS) {
      pwm_pulse_counter = 0;
      pwm_running_counter++;

#if 1
      if (pwm_running_counter % 2) {
        // De-init i2C
        // turn off pwm
        turn_off_pwm_generation();
      } else {
        // turn on pwm
        if (pwm_running_counter != NES_PIR_MEASUREMENT_PWM_WORKING_TIMES) {
          turn_on_pwm_generation();
        }
      }
#endif
#if !NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      if (sensor_data.type == INDICATION_TYPE__Normal_Unconcerned) {
        sensor_data.type = INDICATION_TYPE__Normal_End;
      }
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      // If NES_PIR_MEASUREMENT_PWM_WORKING_TIMES is odd, it enters EM2 already.
      // If NES_PIR_MEASUREMENT_PWM_WORKING_TIMES is even, it needs another EM1 removement.
      if (pwm_running_counter == NES_PIR_MEASUREMENT_PWM_WORKING_TIMES) {
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
        pwm_running_counter = 0;

        // Alter the indication stage component
        if (sensor_data.type == INDICATION_TYPE__Normal_Unconcerned) {
          sensor_data.type = INDICATION_TYPE__Normal_End;
        }

        // enter sleep mode
        // sl_bt_external_signal(NES_PIR_MEASUREMENT_ENTER_SLEEP_MODE_EXTERNAL_SIGNAL_ID);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sc_timer =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sl_simple_timer_start(&nes_pir_measurement_wait_sync_single_timer,
                               NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS,
                               nes_pir_measurement_wait_sync_single_timer_cb,
                               NULL,
                               false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sl_app_assert(sc_timer == SL_STATUS_OK,
                      "[E: 0x%04x] Failed to start wait-sync-single timer\n",
                      (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#else // !NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
        pwm_running_counter = 1;

    #if NES_PIR_MEASUREMENT_ENABLE_SI7021
        nes_sensor_pir_si7021_temperature_humidity_read(
    #if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
            &temperature
    #endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    #if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
            ,
    #endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
    #if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
            &humidity
    #endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
            );

    #if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
        if (sensor_data.has_temp) {
          sensor_data.temp = temperature;
        }
    #endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    #if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
        if (sensor_data.has_humidity) {
          sensor_data.humidity = humidity;
        }
    #endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    #endif // NES_PIR_MEASUREMENT_ENABLE_SI7021
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      }
    }
#else // NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
    if (pwm_running_status == pwm_running_status_on) {
      if (pwm_pulse_counter == NES_PIR_MEASUREMENT_PWM_ON_TIMES) {
        pwm_pulse_counter = 0;
        // turn off pwm
        turn_off_pwm_generation();
      }
    } else if (pwm_running_status == pwm_running_status_off) {
      if (pwm_pulse_counter == NES_PIR_MEASUREMENT_PWM_OFF_TIMES) {
        pwm_pulse_counter = 0;
        // turn off pwm
        turn_on_pwm_generation();
      }
    }

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
    nes_sensor_pir_si7021_temperature_humidity_read(
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
        &temperature
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
        ,
#endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
        &humidity
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
        );

#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    if (sensor_data.has_temp) {
      sensor_data.temp = temperature;
    }
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    if (sensor_data.has_humidity) {
      sensor_data.humidity = humidity;
    }
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021
#endif // NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
    // Send PIR measurement indication to connected client.
    //nes_pir_measurement_pack_sequence_and_indicate(indication_mode_pir_measurement);
    // Print the message.
#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
    nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(nes_pir_measurement_indication_buffer, buffer_length);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))

    // Alter the indication stage component
    if (sensor_data.type == INDICATION_TYPE__Normal_Start) {
      sensor_data.type = INDICATION_TYPE__Normal_Unconcerned;
    }
#if !NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
    else if (sensor_data.type == INDICATION_TYPE__Normal_End) {
      sensor_data.type = INDICATION_TYPE__Normal_Start;
    }
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if !NES_PIR_MEASUREMENT_ENABLE_PWM
    working_time_counter++;
    if (working_time_counter == NES_PIR_MEASUREMENT_GENERAL_WORKING_COUNTERS) {
      working_time_counter = 0;
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sc_timer =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_simple_timer_start(&nes_pir_measurement_wait_sync_single_timer,
                             NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS,
                             nes_pir_measurement_wait_sync_single_timer_cb,
                             NULL,
                             false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_app_assert(sc_timer == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start wait-sync-single timer\n",
                    (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    }
#endif // !NES_PIR_MEASUREMENT_ENABLE_PWM
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
  }
}
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
static void nes_pir_measurement_general_init(void)
{
#if (!NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS) && \
    (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
#if (NES_PIR_MEASUREMENT_ENABLE_PYD1598 || \
    NES_PIR_MEASUREMENT_ENABLE_PWM)
  CMU_ClockEnable(cmuClock_GPIO, true);
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION || NES_PIR_MEASUREMENT_ENABLE_PYD1598)
#endif // (!NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS) ||
       // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
}

static void nes_pir_measurement_general_deinit(void)
{
#if (!NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS) && \
    (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
#if (NES_PIR_MEASUREMENT_ENABLE_PYD1598 || \
    NES_PIR_MEASUREMENT_ENABLE_PWM)
  CMU_ClockEnable(cmuClock_GPIO, false);
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION || NES_PIR_MEASUREMENT_ENABLE_PYD1598)
#endif // (!NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS) ||
       // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION && !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP)
}

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
void nes_pir_measurement_bt_deinit(void)
{
#if !NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
  uint8_t advertising_set_handle = 0xff;
#endif // !NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
#if !NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
  sl_bt_connection_close(open_close_connection);
#endif // NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
}
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

static void nes_pir_measurement_sleep_wakeup_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#if !NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
  uint8_t advertising_set_handle = 0xff;
#endif // !NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION

  is_in_working_mode = true;

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
  // Disable the gpio interrupt
  // TODO: only the order of the below two sentences can work.
  nes_sensor_gpio_interrupt_deinit();
  nes_sensor_gpio_interrupt_disable();
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
  nes_sensor_pir_init();

  // Start timer used for periodic indications.
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_periodic_timer,
                             NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS,
                             nes_pir_measurement_periodic_timer_cb,
                             NULL,
                             true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#else // NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  // Reinit the BLE network
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_bt_advertiser_start(
    advertising_set_handle,
    advertiser_general_discoverable,
    advertiser_connectable_scannable);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start advertising\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#else // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  // Re-init for local testing
  nes_sensor_pir_init();
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#endif // NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
}

static void nes_pir_measurement_wait_sync_single_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  uint8_t i;

  // A timer cannot be removed in its own cb function.
  // Delete the periodic indication timer.
  sl_simple_timer_stop(&nes_pir_measurement_periodic_timer);

  is_in_working_mode = false;

  // Enter EM3 mode totally
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  nes_pir_measurement_bt_deinit();
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  // Deinit peripherals
  nes_pir_measurement_general_deinit();
#if NES_PIR_MEASUREMENT_ENABLE_ADC
  nes_sensor_pir_adc0_deinit();
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

  // Free the element because it will re-allocate again
  if (element) {
    for (i=0; i<NES_PIR_MEASUREMENT_ONE_INDICATION_ELEMENT_NUMBER; i++) {
      free(element[i]);
    }
    free(element);
    element = NULL;
  }

  // Init nes sensor data structure as GPIO interrupt logging
  nes__sensor_data__init(&sensor_data);
#if NES_PIR_MEASUREMENT_ENABLE_PWM
  sensor_data.has_pwm_status = false;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM
#if 0
  sensor_data.sequence_status = sensor_sequence_status;
#endif
  sensor_data.type = INDICATION_TYPE__Interrupt;

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
  nes_sensor_gpio_detection_deinit();
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
  // Change the GPIO level detection to be interruption
  nes_sensor_gpio_interrupt_init();
  nes_sensor_gpio_interrupt_enable();
  gpio_interruption_valid = false;
  // Start the valid check timer
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_sensor_gpio_interrupt_valid_check_timer,
                        (NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_VALID_INTERVAL_SECONDS * 1000),
                        nes_sensor_gpio_interrupt_valid_check_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start sleep wakeup timer\n",
                (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

  // Init the wake up timer
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_sleep_wakeup_timer,
                        (NES_PIR_MEASUREMENT_SLEEP_INTERVAL_SECONDS * 1000),
                         nes_pir_measurement_sleep_wakeup_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start sleep wakeup timer\n",
                (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

#if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
  if ((NES_PIR_MEASUREMENT_PWM_WORKING_TIMES % 2) == 0) {
    // Exit EM1 mode bonding.
    sl_power_manager_remove_em_requirement(SL_POWER_MANAGER_EM1);
  }
#endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
}
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

static void nes_sensor_pir_init(void)
{
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

  nes_sensor_pir_communication_init_sensor_data_structure();

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
  nes_pir_measurement_general_init();
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
  nes_sensor_gpio_detection_init();
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_ADC
  // Init ADC0
  nes_sensor_pir_adc0_init();
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
  nes_sensor_pir_pyd1598_init(NES_PIR_MEASUREMENT_PYD1598_BAND_PASS_INIT);
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
  nes_sensor_pir_peripheral_init_extend_gpios();
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS

#if NES_PIR_MEASUREMENT_ENABLE_PWM
#if NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
  pwm_running_status = pwm_running_status_none;
#endif // NES_PIR_MEASUREMENT_ENABLE_SEPARATE_PWM_ON_OFF_SCHEDULE
  // Init for state machine
  pwm_pulse_counter = 0;
  pwm_running_counter = 0;
  turn_on_pwm_generation();
#if (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_LETIMER) && \
    (NES_PIR_MEASUREMENT_PWM1_MODE == NES_PIR_MEASUREMENT_PWM1_MODE_LETIMER)

#endif // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_LETIMER) &&
       // (NES_PIR_MEASUREMENT_PWM1_MODE == NES_PIR_MEASUREMENT_PWM1_MODE_LETIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  // Start timer used for periodic indications.
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_periodic_timer,
                         NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS,
                         nes_pir_measurement_periodic_timer_cb,
                         NULL,
                         true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
}

static void nes_sensor_init(void)
{
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  uint16_t measurement_interval = NES_PIR_MEASUREMENT_INDICATION_INTERVAL_MS;

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_bt_gatt_server_write_attribute_value(gattdb_measurement_interval,
                                           0,
                                           sizeof(measurement_interval),
                                           (uint8_t *)&measurement_interval);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to write attribute\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
  is_in_working_mode = true;
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
  // Initialize watchdog timer
  WDOG_Init(&wdg_init);
#if (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_watchdog_feed_timer,
                         (1000 * NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS),
                         nes_pir_measurement_watchdog_feed_timer_cb,
                         NULL,
                         true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#elif ((NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER))
  sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS),
                              NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE,
                              0);
#endif // (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_smart_advertising_working_timer,
                         (1000 * NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS),
                         nes_pir_measurement_smart_advertising_working_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#elif (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
  sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS),
                              NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE,
                              1);
#endif // (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)

#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_periodic_register_timer,
                         (1000 * NES_PIR_MEASUREMENT_REGISTER_PERIODICAL_TIMER_INTERVAL_SECONDS),
												 nes_pir_measurement_periodic_register_timer_cb,
                         NULL,
                         true);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

  nes_sensor_peripheral_init();

  running_mode = running_mode_none;
  collecting_sequence = MINIMUM_SEQUENCE_NUMBER;
  power_detection_sequence = MINIMUM_SEQUENCE_NUMBER;
  testing_sequence = MINIMUM_SEQUENCE_NUMBER;
  packet_status = packet_status_none;

  // If we enter the nes_collecting mode
  nes_pir_measurement_indication_buffer[0] = NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_COLLECTED_DATA;

  sensor_sequence_status = (SequenceStatus*)malloc(sizeof(SequenceStatus));
  if (sensor_sequence_status) {
    sequence_status__init(sensor_sequence_status);
    sensor_sequence_status->packet_loss = false;
  }

#if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
  is_in_low_power_mode = true;
#endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)

#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  nes_sensor_pir_init();
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
}

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
static void nes_bt_sensor_measurement_connection_closed_cb(uint16_t reason, uint8_t connection)
{
  (void)connection;

  if_ok_to_indicate = false;
#if defined(SL_APP_LOG_ENABLE) && SL_APP_LOG_ENABLE
  sl_status_print(reason);
#endif // defined(SL_APP_LOG_ENABLE) && SL_APP_LOG_ENABLE
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  nes_sensor_server_log("The disconnection reason is %u. ", reason);
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  switch (reason) {
    case SL_STATUS_BT_CTRL_CONNECTION_TERMINATED_BY_LOCAL_HOST:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      nes_sensor_server_log("The connection is down because of the server.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      break;
    case SL_STATUS_BT_CTRL_LL_RESPONSE_TIMEOUT:
    case SL_STATUS_BT_CTRL_CONNECTION_TERMINATED_DUE_TO_MIC_FAILURE:
    case SL_STATUS_BT_CTRL_CONNECTION_TIMEOUT:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      nes_sensor_server_log("The connection is down because of the client.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
    #if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
      if (!is_in_low_power_mode) {
#if NES_PIR_MEASUREMENT_ENABLE_PWM
        turn_off_pwm_generation();
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM
      }
    #endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      // Turn down power consuming peripherals
      // Deinit peripherals
      nes_pir_measurement_general_deinit();
    #if NES_PIR_MEASUREMENT_ENABLE_ADC
      nes_sensor_pir_adc0_deinit();
    #endif // NES_PIR_MEASUREMENT_ENABLE_ADC
    #if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
      nes_sensor_gpio_detection_deinit();
    #endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
      if (is_in_working_mode) {
        counter = 0;
        #if NES_PIR_MEASUREMENT_ENABLE_PWM
        pwm_pulse_counter = 0;
        pwm_running_counter = 0;
        #endif // NES_PIR_MEASUREMENT_ENABLE_PWM

        #if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
        #if !NES_PIR_MEASUREMENT_ENABLE_PWM
        uint8_t working_time_counter = 0;
        #endif // NES_PIR_MEASUREMENT_ENABLE_PWM
        #endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

      #if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
      #if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sl_status_t sc;
      #endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      #endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

        // Stop all the timers.
      #if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
      #if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sc =
      #endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sl_simple_timer_stop(&nes_pir_measurement_periodic_timer);
      #if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
        sl_app_assert(sc == SL_STATUS_OK,
                      "[E: 0x%04x] Failed to stop periodic timer\n",
                      (int)sc);
      #endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      #endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
      } else {
  #if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
  #if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
  #if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
        nes_sensor_gpio_interrupt_deinit();
        nes_sensor_gpio_interrupt_disable();
  #endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
  #endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
  #endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
      }
#else // !NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
    #if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_status_t sc;
    #endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    #endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

      // Stop all the timers.
    #if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
    #if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sc =
    #endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_simple_timer_stop(&nes_pir_measurement_periodic_timer);
    #if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to stop periodic timer\n",
                    (int)sc);
    #endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    #endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
      nes_pir_measurement_smart_advertising_enter();
#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
      break;
    default:
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      nes_sensor_server_log("The connection is down because of the others.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
      break;
  }
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  nes_sensor_server_log("\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
}
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#if (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
static void nes_pir_measurement_watchdog_feed_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;

  WDOG_Feed();
}
#endif // (NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE == NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
static void nes_pir_measurement_periodic_register_timer_cb(sl_simple_timer_t *timer, void *data)
{
	(void)timer;
	(void)data;

	if (if_ok_to_indicate) {
		// Indicate node status
		nes_pir_measurement_pack_sequence_and_indicate(indication_mode_register);
	}
}
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING
static void nes_pir_measurement_smart_advertising_enter(void)
{
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

  // Start general advertising and enable connections.
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_bt_advertiser_start(
    advertising_set_handle,
    advertiser_general_discoverable,
    advertiser_connectable_scannable);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start advertising\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  nes_sensor_server_log("Start advertising\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)

#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_smart_advertising_working_timer,
                         (1000 * NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS),
                         nes_pir_measurement_smart_advertising_working_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#elif (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER)
  sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS),
                              NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_TIMER_HANDLE,
                              1);
#endif // (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
}

#if (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
static void nes_pir_measurement_smart_advertising_working_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
  nes_sensor_server_log("Working advertising is timeout.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
  // Stop advertising
  sl_bt_advertiser_stop(advertising_set_handle);

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_smart_advertising_sleeping_timer,
                        (1000 * NES_PIR_MEASUREMENT_SMART_ADVERTISING_SLEEPING_PERIOD_SECONDS),
                         nes_pir_measurement_smart_advertising_sleeping_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
}

static void nes_pir_measurement_smart_advertising_sleeping_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
  nes_sensor_server_log("Sleeping advertising is timeout.\n");
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW)
  // Start advertising
  sl_bt_advertiser_start(
    advertising_set_handle,
    advertiser_general_discoverable,
    advertiser_connectable_scannable);

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_pir_measurement_smart_advertising_working_timer,
                        (1000 * NES_PIR_MEASUREMENT_SMART_ADVERTISING_WORKING_PERIOD_SECONDS),
                         nes_pir_measurement_smart_advertising_working_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start periodic timer\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
}
#endif // (NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE == NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_SMART_ADVERTISING

#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

void nes_pir_measurement_pack_sequence_and_indicate(indication_mode_t indication_mode)
{
#if NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
  sl_status_t sc;
  uint16_t len = 0;
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

  switch (indication_mode) {
    case indication_mode_pir_measurement:
    case indication_mode_pir_sleep_measurement:
      nes_pir_measurement_indication_buffer[0] = NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_COLLECTED_DATA;
#if 0
      sensor_data.sequence_status->current_sequence = collecting_sequence;
#else
      sensor_data.sequence = collecting_sequence;
#endif
      buffer_length = nes__sensor_data__pack(&sensor_data, &nes_pir_measurement_indication_buffer[1]);
      buffer_length++;
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
      // Send PIR measurement indication to connected client.
      sc = sl_bt_gatt_server_send_characteristic_notification(
        app_connection,
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        (indication_mode == indication_mode_pir_measurement) ? (gattdb_pir_measurement) : (gattdb_pir_sleep_measurement),
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        gattdb_pir_measurement,
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        buffer_length,
        nes_pir_measurement_indication_buffer,
        &len);
        if (sc) {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
          nes_sensor_server_log("Warning! Failed to send characteristic notification(sc = %lu)\n", sc);
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
#if 0
          // Just treat as disconnection??
          if (sensor_data.sequence_status->packet_loss == false) {
            sensor_data.sequence_status->packet_loss = true;
            sensor_data.sequence_status->blocked_sequence = collecting_sequence;
          }
#endif
        } else {
#if 0
          if (sensor_data.sequence_status->packet_loss == true) {
            sensor_data.sequence_status->packet_loss = false;
          }
#endif
          packet_status = packet_status_sent;
        }

        collecting_sequence++;
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
      break;
    case indication_mode_testing:

      break;
    case indication_mode_register:
      {
        NESSensorRegisterStatus sensor_register;
        nes__sensor_register_status__init(&sensor_register);
        sensor_register.status = node_status;
        nes_pir_measurement_indication_buffer[0] = NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_NODE_STATUS;
        buffer_length = nes__sensor_register_status__pack(&sensor_register, &nes_pir_measurement_indication_buffer[1]);
        buffer_length++;
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
      // Send PIR measurement indication to connected client.
      sc = sl_bt_gatt_server_send_characteristic_notification(
        app_connection,
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        (indication_mode == indication_mode_pir_measurement) ? (gattdb_pir_measurement) : (gattdb_pir_sleep_measurement),
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        gattdb_pir_measurement,
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        buffer_length,
        nes_pir_measurement_indication_buffer,
        &len);
        if (sc) {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
          nes_sensor_server_log("Warning! Failed to send characteristic notification(sc = %lu)\n", sc);
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
        }
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
      }
      break;
    default:
      break;
  }
#endif // NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
}
