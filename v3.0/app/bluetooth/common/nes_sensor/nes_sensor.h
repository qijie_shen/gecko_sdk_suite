/***************************************************************************//**
 * @file
 * @brief NES Sensor GATT service
 *******************************************************************************
 * # License
 * <b>Copyright </b>
 *******************************************************************************
 *
 *
 ******************************************************************************/

#ifndef NES_SENSOR_H
#define NES_SENSOR_H

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#include "sl_status.h"
#include "sl_bt_types.h"
#include "nes_sensor_config.h"

/**************************************************************************//**
 * @todo
 * We have only PIR testing requirement right now.
 * In the future, it's necessary to split NES sensor into specific slcc files.
 *****************************************************************************/

#if NES_PIR_MEASUREMENT_LOG_ENABLE
#define nes_sensor_server_log(...)                                            printf(__VA_ARGS__)
#else // NES_PIR_MEASUREMENT_LOG_ENABLE
#define nes_sensor_server_log(...)
#endif // NES_PIR_MEASUREMENT_LOG_ENABLE

/// Possible NES sensor run-time logging modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL                                0
#define NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS                             1
#define NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL                  2

/// Possible NES sensor run-time energy modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_ENERGY_MODE_EM1                                   0
#define NES_PIR_MEASUREMENT_ENERGY_MODE_EM2                                   1
#define NES_PIR_MEASUREMENT_ENERGY_MODE_EM3                                   2

/// Possible NES sensor timer modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE                                 0
#define NES_PIR_MEASUREMENT_TIMER_MODE_SLEEP                                  1
#define NES_PIR_MEASUREMENT_TIMER_MODE_LETIMER                                2
#define NES_PIR_MEASUREMENT_TIMER_MODE_STD_TIMER                              3
#define NES_PIR_MEASUREMENT_TIMER_MODE_STD_WTIMER                             4

/// Possible NES sensor PWM0 modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_PWM0_MODE_NONE                                    0
#define NES_PIR_MEASUREMENT_PWM0_MODE_LETIMER                                 1
#define NES_PIR_MEASUREMENT_PWM0_MODE_TIMER                                   2
#define NES_PIR_MEASUREMENT_PWM0_MODE_WTIMER                                  3

/// Possible NES sensor PWM1 modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_PWM1_MODE_NONE                                    0
#define NES_PIR_MEASUREMENT_PWM1_MODE_LETIMER                                 1
#define NES_PIR_MEASUREMENT_PWM1_MODE_TIMER                                   2
#define NES_PIR_MEASUREMENT_PWM1_MODE_WTIMER                                  3

/// Possible NES sensor ADC running modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE                           0
#define NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN                             1

/// Possible NES sensor data communication modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_RAW                       0
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_JSON                      1
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_PROTOBUF                  2

/// Possible Watchdog timeout period selection
/// Can only use macros
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_1_SECOND                  1
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND                  2
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_4_SECOND                  4
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_8_SECOND                  8
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND                 16
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_32_SECOND                 32
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_64_SECOND                 64
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_128_SECOND                128
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_256_SECOND                256

/// Possible smart advertising timer mode selection
/// Can only use macros
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SYSTEM_SOFT_TIMER    0
#define NES_PIR_MEASUREMENT_SMART_ADVERTISING_TIMER_MODE_SIMPLE_TIMER         1

/// Possible smart advertising timer mode selection
/// Can only use macros
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SYSTEM_SOFT_TIMER        0
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_MODE_SIMPLE_TIMER             1

// Define the indication buffer length
#define NES_PIR_MEASUREMENT_INDICATION_BUFFER_LENGTH_BYTES                    300

// Define ADC channels' positions
#define NES_PIR_MEASUREMENT_ADC_CHANNEL0_POSITION                             adcPosSelAPORT2XCH9

#define NES_PIR_MEASUREMENT_ADC_CHANNEL1_POSITION                             adcPosSelAPORT2YCH20

#define NES_PIR_MEASUREMENT_ADC_CHANNEL2_POSITION                             adcPosSelAPORT1YCH21

#define NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_COLLECTED_DATA               'C'
#define NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_POWER_STATUS                 'P'
#define NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_TESTING                      'T'
#define NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_HEART_BEAT_MSG               'H'
#define NES_PIR_MEASUREMENT_INDICATION_HEAD_CHAR_NODE_STATUS                  'R'

#define TICKS_TO_DELAY_ONE_SECOND                                             32768

typedef enum
{
  pir_measurement_mode_working            = 0x0, /**< (0x0) Discover only limited
                                           discoverable devices. */
  pir_measurement_mode_working_sync       = 0x1, /**< (0x0) Discover only limited
                                           discoverable devices. */
  pir_measurement_mode_sleeping           = 0x2, /**< (0x1) Discover limited and generic
                                           discoverable devices. */
  pir_measurement_mode_sleeping_sync      = 0x3, /**< (0x1) Discover limited and generic
                                           discoverable devices. */
  pir_measurement_mode_none               = 0x4
} pir_measurement_mode_t;

typedef enum
{
  running_mode_sensor                     = 0x0,
  running_mode_testing                    = 0x1,
  running_mode_none                       = 0x2
}running_mode_t;

typedef enum
{
  packet_status_sent                      = 0x0,
  packet_status_confirmed                 = 0x1,
  packet_status_none                      = 0x2
}packet_status_t;

typedef enum
{
  indication_mode_pir_measurement         = 0x0,
  indication_mode_pir_sleep_measurement   = 0x1,
  indication_mode_testing                 = 0x2,
  indication_mode_register                = 0x3,
}indication_mode_t;

typedef enum
{
  pwm_running_status_on                   = 0x0,
  pwm_running_status_off                  = 0x1,
  pwm_running_status_none                 = 0x2
}pwm_running_status_t;

#define TESTING_MODE_STATUS_NUMBER        0

#define MINIMUM_SEQUENCE_NUMBER           0
#define MAXIMUM_SEQUENCE_NUMBER           4294967295

#define NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_OFF                              0
#define NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_RAW                              1
#define NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG                            2
#define NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF                         3
#define NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA               4
/**************************************************************************//**
 * Send PIR Measurement characteristic indication to the client.
 * @param[in] connection Connection handle of the client.
 * @param[in] value PIR value in millidegree.
 * @param[in] fahrenheit Value is given in Fahrenheit (true) or Celsius (false).
 *****************************************************************************/
sl_status_t nes_bt_sensor_pir_measurement_indicate(uint8_t connection,
                                                     uint32_t adc_value,
                                                     uint8_t gpio_value,
                                                     bool pwm_enabled);

/**************************************************************************//**
 * PIR Measurement characteristic's CCCD has changed.
 * @param[in] connection Connection handle of the client.
 * @param[in] client_config Characteristic Client Configuration Flag.
 * @note To be implemented in user code.
 *****************************************************************************/
void nes_bt_sensor_measurement_indication_changed_cb(uint8_t connection,
                                                            gatt_client_config_flag_t client_config);

/**************************************************************************//**
 * PIR Sleep Measurement characteristic's CCCD has changed.
 * @param[in] connection Connection handle of the client.
 * @param[in] client_config Characteristic Client Configuration Flag.
 * @note To be implemented in user code.
 *****************************************************************************/
void nes_bt_sensor_sleep_measurement_indication_changed_cb(uint8_t connection,
                                                            gatt_client_config_flag_t client_config);

/**************************************************************************//**
 * PIR Measurement characteristic indication confirmed.
 * @param[in] connection Connection handle of the client.
 * @note To be implemented in user code.
 *****************************************************************************/
void nes_bt_sensor_measurement_indication_confirmed_cb(uint8_t connection);

/**************************************************************************//**
 * Bluetooth stack event handler.
 * @param[in] evt Event coming from the Bluetooth stack.
 *****************************************************************************/
void nes_bt_sensor_measurement_on_event(sl_bt_msg_t *evt);

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
void nes_pir_measurement_bt_deinit(void);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

void nes_pir_measurement_pack_sequence_and_indicate(indication_mode_t indication_mode);

#endif // NES_SENSOR_H
