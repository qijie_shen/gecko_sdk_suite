#include "sl_app_assert.h"
#include "sl_bluetooth.h"
#include "nes_sensor.h"
#include "nes_sensor_communication.h"
//#include "nes_sensor_peripheral.h"

/// Header files
#if defined(SL_CATALOG_POWER_MANAGER_PRESENT)
#include "sl_power_manager.h"
#endif // SL_CATALOG_POWER_MANAGER_PRESENT

#if (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)
#include "sl_simple_timer.h"
#endif // (NES_PIR_MEASUREMENT_INDICATION_TIMER_MODE == NES_PIR_MEASUREMENT_TIMER_MODE_SIMPLE)

#if NES_PIR_MEASUREMENT_ENABLE_ADC
#include "em_adc.h"
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PWM
#if (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER) || \
    (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
#include "em_timer.h"
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER) ||
       // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION || \
    NES_PIR_MEASUREMENT_ENABLE_PYD1598 || \
    NES_PIR_MEASUREMENT_ENABLE_PWM || \
    NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS || \
    NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS )
#include "em_gpio.h"
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION)
       // || NES_PIR_MEASUREMENT_ENABLE_PYD1598
       // || NES_PIR_MEASUREMENT_ENABLE_PWM
       // || NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
       // || NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS)

#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION \
  || NES_PIR_MEASUREMENT_ENABLE_PYD1598 \
  || NES_PIR_MEASUREMENT_ENABLE_ADC)
#include "em_cmu.h"
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
       // || NES_PIR_MEASUREMENT_ENABLE_PYD1598
       // || NES_PIR_MEASUREMENT_ENABLE_ADC)

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
#include "sl_udelay.h"
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#include "gpiointerrupt.h"
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
#include "sl_si70xx.h"
#include "sl_i2cspm_instances.h"
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
extern uint8_t advertising_set_handle;
#endif // NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

/// Static variable definitions
#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
#if NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO
static uint32_t serial_in_set_addr;
static uint32_t serial_in_clear_addr;
static uint32_t serial_in_mask;

static uint32_t dl_set_addr;
static uint32_t dl_clear_addr;
static uint32_t dl_mask;
static uint32_t dl_read_addr;
#endif // NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_PWM
bool pwm_is_running;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
extern bool is_in_low_power_mode;
#endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
bool gpio_interruption_valid;
sl_simple_timer_t nes_sensor_gpio_interrupt_valid_check_timer;
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
sl_simple_timer_t nes_sensor_gpio_interrupt_wait_sync_single_timer;
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
uint32_t node_status;
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

/// Programming specific definitions
#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
// Configure PA2 as input
// P0 on the WSTK board
#define STATUS_DEFINITION_0_MODE_INPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_STATUS_DEFINITION_0_PORT, NES_PIR_MEASUREMENT_STATUS_DEFINITION_0_PIN, gpioModeInput, 0);

// Configure PA3 as input
// P2 on the WSTK board
#define STATUS_DEFINITION_1_MODE_INPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_STATUS_DEFINITION_1_PORT, NES_PIR_MEASUREMENT_STATUS_DEFINITION_1_PIN, gpioModeInput, 0);

// Configure PC7 as input
// P3 on the WSTK board
#define STATUS_DEFINITION_2_MODE_INPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_STATUS_DEFINITION_2_PORT, NES_PIR_MEASUREMENT_STATUS_DEFINITION_2_PIN, gpioModeInput, 0);

#define STATUS_DEFINITION_0_GET() \
  GPIO_PinInGet(NES_PIR_MEASUREMENT_STATUS_DEFINITION_0_PORT, NES_PIR_MEASUREMENT_STATUS_DEFINITION_0_PIN)

#define STATUS_DEFINITION_1_GET() \
  GPIO_PinInGet(NES_PIR_MEASUREMENT_STATUS_DEFINITION_1_PORT, NES_PIR_MEASUREMENT_STATUS_DEFINITION_1_PIN)

#define STATUS_DEFINITION_2_GET() \
  GPIO_PinInGet(NES_PIR_MEASUREMENT_STATUS_DEFINITION_2_PORT, NES_PIR_MEASUREMENT_STATUS_DEFINITION_2_PIN)
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
// Configure PC9 as output
// P7 on the WSTK board
#define PYD1598_SERIAL_IN_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PORT, NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PIN, gpioModePushPull, 0);

// Configure PC10 as output
// P12 on the WSTK board
#define PYD1598_DL_MODE_INPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PYD1598_DL_PORT, NES_PIR_MEASUREMENT_PYD1598_DL_PIN, gpioModeInput, 0);

#define PYD1598_DL_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PYD1598_DL_PORT, NES_PIR_MEASUREMENT_PYD1598_DL_PIN, gpioModePushPull, 0);

#if NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO

#define PYD1598_SERIAL_IN_CLEAR(); \
  *(volatile uint32_t *)serial_in_clear_addr = serial_in_mask;

#define PYD1598_SERIAL_IN_SET(); \
  *(volatile uint32_t *)serial_in_set_addr = serial_in_mask;

#define PYD1598_DL_CLEAR(); \
  *(volatile uint32_t *)dl_clear_addr = dl_mask;

#define PYD1598_DL_SET(); \
  *(volatile uint32_t *)dl_set_addr = dl_mask;

#define PYD1598_DL_GET() \
  *(volatile uint32_t *)dl_read_addr

#else // !NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO
#define PYD1598_SERIAL_IN_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PORT, NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PIN)

#define PYD1598_SERIAL_IN_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PORT, NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PIN)

#define PYD1598_DL_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_PYD1598_DL_PORT, NES_PIR_MEASUREMENT_PYD1598_DL_PIN)

#define PYD1598_DL_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_PYD1598_DL_PORT, NES_PIR_MEASUREMENT_PYD1598_DL_PIN)

#define PYD1598_DL_GET() \
  GPIO_PinInGet(NES_PIR_MEASUREMENT_PYD1598_DL_PORT, NES_PIR_MEASUREMENT_PYD1598_DL_PIN)
#endif // NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO

#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
#define EXTEND_DRIVE_GPIO_PIR_0_MODE_DISABLE() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PIN, gpioModeDisabled, 0);

#define EXTEND_DRIVE_GPIO_PIR_0_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PIN, gpioModePushPull, 0);

#define EXTEND_DRIVE_GPIO_PIR_0_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PIN)

#define EXTEND_DRIVE_GPIO_PIR_0_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_0_PIN)

#define EXTEND_DRIVE_GPIO_PIR_1_MODE_DISABLE() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PIN, gpioModeDisabled, 0);

#define EXTEND_DRIVE_GPIO_PIR_1_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PIN, gpioModePushPull, 0);

#define EXTEND_DRIVE_GPIO_PIR_1_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PIN)

#define EXTEND_DRIVE_GPIO_PIR_1_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_1_PIN)

#define EXTEND_DRIVE_GPIO_PIR_2_MODE_DISABLE() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PIN, gpioModeDisabled, 0);

#define EXTEND_DRIVE_GPIO_PIR_2_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PIN, gpioModePushPull, 0);

#define EXTEND_DRIVE_GPIO_PIR_2_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PIN)

#define EXTEND_DRIVE_GPIO_PIR_2_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_PIR_2_PIN)

#define EXTEND_DRIVE_GPIO_REGULATOR_0_MODE_DISABLE() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PIN, gpioModeDisabled, 0);

#define EXTEND_DRIVE_GPIO_REGULATOR_0_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PIN, gpioModePushPull, 0);

#define EXTEND_DRIVE_GPIO_REGULATOR_0_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PIN)

#define EXTEND_DRIVE_GPIO_REGULATOR_0_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_0_PIN)

#define EXTEND_DRIVE_GPIO_REGULATOR_1_MODE_DISABLE() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PIN, gpioModeDisabled, 0);

#define EXTEND_DRIVE_GPIO_REGULATOR_1_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PIN, gpioModePushPull, 0);

#define EXTEND_DRIVE_GPIO_REGULATOR_1_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PIN)

#define EXTEND_DRIVE_GPIO_REGULATOR_1_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_1_PIN)

#define EXTEND_DRIVE_GPIO_REGULATOR_2_MODE_DISABLE() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PIN, gpioModeDisabled, 0);

#define EXTEND_DRIVE_GPIO_REGULATOR_2_MODE_OUTPUT() \
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PIN, gpioModePushPull, 0);

#define EXTEND_DRIVE_GPIO_REGULATOR_2_SET() \
  GPIO_PinOutSet(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PIN)

#define EXTEND_DRIVE_GPIO_REGULATOR_2_CLEAR() \
  GPIO_PinOutClear(NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PORT, NES_PIR_MEASUREMENT_EXTEND_DRIVE_GPIO_REGULATOR_2_PIN)
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
void nes_sensor_pir_peripheral_init_extend_gpios(void);
static void nes_sensor_pir_peripheral_clear_extend_gpios(void);
static void nes_sensor_pir_peripheral_set_extend_gpios(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
static void nes_sensor_pir_si7021_temperature_init(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

void nes_sensor_peripheral_init(void)
{
#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
  unsigned int status_0;
  unsigned int status_1;
  unsigned int status_2;
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

#if (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION || \
    NES_PIR_MEASUREMENT_ENABLE_PYD1598 || \
    NES_PIR_MEASUREMENT_ENABLE_PWM || \
    NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS || \
    NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS)
  CMU_ClockEnable(cmuClock_GPIO, true);
#endif // (NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
       // || NES_PIR_MEASUREMENT_ENABLE_PYD1598
       // || NES_PIR_MEASUREMENT_ENABLE_PWM
       // || NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
       // || NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS)

#if NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS
  node_status = 0;

  // Init three status GPIOs as inputs
  STATUS_DEFINITION_0_MODE_INPUT();
  STATUS_DEFINITION_1_MODE_INPUT();
  STATUS_DEFINITION_2_MODE_INPUT();

  status_0 = STATUS_DEFINITION_0_GET();
  status_1 = STATUS_DEFINITION_1_GET();
  status_2 = STATUS_DEFINITION_2_GET();

  // P0 << 2 + P2 << 1 + P3 << 0
  node_status = (status_0 << 2) + \
                (status_1 << 1) + \
                (status_2 << 0);
/*
  nes_sensor_server_log("status_0 is %u, status_1 = %u, status_2 = %u\nnode_status is %lu\n\n",
             status_0, \
             status_1, \
             status_2, \
             node_status);
*/
#endif // NES_PIR_MEASUREMENT_ENABLE_STATUS_VIA_GPIOS

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
  nes_sensor_pir_si7021_temperature_init();
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021
}

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION
void nes_sensor_gpio_detection_init(void)
{
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, \
                  NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, gpioModeInput, 0);
}

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
void nes_sensor_gpio_detection_deinit(void)
{
  // De-Configure PF7 as input
  // P36 on the WSTK board
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, \
                  NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, gpioModeDisabled, 0);
}
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

unsigned int nes_sensor_gpio_detection_read(void)
{
  return GPIO_PinInGet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, \
                       NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN);
}

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
void nes_sensor_gpio_interrupt_valid_check_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;

  gpio_interruption_valid = true;
}

void nes_sensor_gpio_interrupt_enable(void)
{
  GPIO_ExtIntConfig(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT,
                    NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN,
                    NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN,
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE
                    false,
                    true,
#else //!NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE
                    true,
                    false,
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE
                    true);
}

void nes_sensor_gpio_interrupt_disable(void)
{
  // TODO: The last parameter is set to be false will tigger the corresponding callback function?!
  GPIO_ExtIntConfig(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT,
                    NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN,
                    NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN,
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE
                    false,
                    true,
#else //!NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE
                    true,
                    false,
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_TRIGGER_EDGE
                    false);
}

void nes_sensor_gpio_interrupt_wait_sync_single_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

  // Enter EM3 mode totally
  nes_pir_measurement_bt_deinit();

  nes_sensor_gpio_interrupt_enable();
  // Start the valid check timer
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_sensor_gpio_interrupt_valid_check_timer,
                        (NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_VALID_INTERVAL_SECONDS * 1000),
                        nes_sensor_gpio_interrupt_valid_check_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start sleep wakeup timer\n",
                (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
}

void nes_sensor_gpio_interrupt_cb(uint8_t pin)
{
  (void)pin;
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#if !NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
  uint8_t advertising_set_handle = 0xff;
#endif // !NES_PIR_MEASUREMENT_ENABLE_NETWORK_SOLUTION
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)

  // Disable the gpio interruption
  nes_sensor_gpio_interrupt_disable();

  // Check for the elapsed time larger than the min interval
  if (gpio_interruption_valid == true) {
    // Wake up the network
#if NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE
  // Send PIR measurement indication to connected client.
  // Indication or notification enabled.
  nes_pir_measurement_pack_sequence_and_indicate(indication_mode_pir_sleep_measurement);

  // Print the message.
#if ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || \
    (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL)
  nes_sensor_pir_communication_unpack_nes_sensor_data_and_print(nes_pir_measurement_indication_buffer, buffer_length);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_WIRELESS_WITH_LOCAL)
#endif // ((NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF) || (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_PROTOBUF_PURE_DATA))

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc_timer =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_simple_timer_start(&nes_sensor_gpio_interrupt_wait_sync_single_timer,
                         NES_PIR_MEASUREMENT_WAIT_BEFORE_ENTER_LOW_POWER_MODE_INTERVAL_MS,
                         nes_sensor_gpio_interrupt_wait_sync_single_timer_cb,
                         NULL,
                         false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc_timer == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start wait-sync-single timer\n",
                (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

#else

#if (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
    // Reinit the BLE network
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_bt_advertiser_start(
      advertising_set_handle,
      advertiser_general_discoverable,
      advertiser_connectable_scannable);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start advertising\n",
                  (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE != NES_PIR_MEASUREMENT_LOGGING_MODE_LOCAL)
#endif // NES_PIR_MEASUREMENT_ENABLE_VIRTUAL_SLEEP_MODE

    gpio_interruption_valid = false;
    // Re-start the interrupt valid check timer
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_simple_timer_start(&nes_sensor_gpio_interrupt_valid_check_timer,
                          (NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_INTERRUPT_VALID_INTERVAL_SECONDS * 1000),
                          nes_sensor_gpio_interrupt_valid_check_timer_cb,
                          NULL,
                          false);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start sleep wakeup timer\n",
                  (int)sc_timer);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  }

  // Re-enable the interruption
  nes_sensor_gpio_interrupt_enable();
}

void nes_sensor_gpio_interrupt_init(void)
{
  // Good pull-up/down selection
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT,
                  NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, gpioModeInputPullFilter, 1);
#else // !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, \
                    NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, gpioModeInputPull, 0);
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1

  // Enable interrupt.
  // Check if the interruption pin is odd or even
  if (NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN % 2) {
    // Odd
    NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
    NVIC_EnableIRQ(GPIO_ODD_IRQn);
  } else {
    // Even
    NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  }

  GPIOINT_CallbackRegister(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, nes_sensor_gpio_interrupt_cb);
}

void nes_sensor_gpio_interrupt_deinit(void)
{
  // Configure PF7 as disabled.
  // TODO: this code below will trigger the interrupt callback !!!
  // GPIO_PinModeSet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, gpioModeDisabled, 1);

  // Disable interrupt.
  // Check if the interruption pin is odd or even
  if (NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN % 2) {
    // Odd
    NVIC_DisableIRQ(GPIO_ODD_IRQn);
  } else {
    // Even
    NVIC_DisableIRQ(GPIO_EVEN_IRQn);
  }
}
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_ENABLE_SLEEP_WAKE_UP
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_LEVEL_DETECTION

#if NES_PIR_MEASUREMENT_ENABLE_ADC
void nes_sensor_pir_adc0_init(void)
{
#if (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  CMU_ClockEnable(cmuClock_HFPER, true);
#endif // (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  // Enable ADC0 clock
  CMU_ClockEnable(cmuClock_ADC0, true);

  // Declare init structs
  ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
#if (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE)
  ADC_InitSingle_TypeDef initSingle = ADC_INITSINGLE_DEFAULT;

  initSingle.diff       = false;        // single ended
  initSingle.reference  = adcRefVDD;    // 3.3V reference
  initSingle.resolution = adcRes12Bit;  // 12-bit resolution
  initSingle.acqTime    = adcAcqTime4;  // set acquisition time to meet minimum requirement

  // Select ADC input. See README for corresponding EXP header pin.
  initSingle.posSel = adcPosSelAPORT2XCH9;  // PC9 Pin, P7 on the WSTK board
#elif (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  ADC_InitScan_TypeDef initScan = ADC_INITSCAN_DEFAULT;

  initScan.diff          = false;       // single ended
  initScan.reference     = adcRefVDD;   // internal 2.5V reference
  initScan.resolution    = adcRes12Bit; // 12-bit resolution
  initScan.acqTime       = adcAcqTime4; // set acquisition time to meet minimum requirements
  initScan.fifoOverwrite = true;        // FIFO overflow overwrites old data

  // Select ADC input. See README for corresponding EXP header pin.
  // *Note that internal channels are unavailable in ADC scan mode

#if NES_PIR_MEASUREMENT_ADC_NUM
  // PC9 -> P7
  ADC_ScanSingleEndedInputAdd(&initScan, adcScanInputGroup0, NES_PIR_MEASUREMENT_ADC_CHANNEL0_POSITION);
#if (NES_PIR_MEASUREMENT_ADC_NUM > 1)
  // PF4 -> P30
  ADC_ScanSingleEndedInputAdd(&initScan, adcScanInputGroup1, NES_PIR_MEASUREMENT_ADC_CHANNEL1_POSITION);
#if (NES_PIR_MEASUREMENT_ADC_NUM > 2)
  // PF5 -> P32
  ADC_ScanSingleEndedInputAdd(&initScan, adcScanInputGroup2, NES_PIR_MEASUREMENT_ADC_CHANNEL2_POSITION);
#endif // (NES_PIR_MEASUREMENT_ADC_NUM > 2)
#endif // (NES_PIR_MEASUREMENT_ADC_NUM > 1)
#endif // NES_PIR_MEASUREMENT_ADC_NUM

  // Set scan data valid level (DVL) to 2
  ADC0->SCANCTRLX = (ADC0->SCANCTRLX & ~_ADC_SCANCTRLX_DVL_MASK) | (((NES_PIR_MEASUREMENT_ADC_NUM - 1) << _ADC_SCANCTRLX_DVL_SHIFT) & _ADC_SCANCTRLX_DVL_MASK);

  // Clear ADC Scan FIFO
  ADC0->SCANFIFOCLEAR = ADC_SCANFIFOCLEAR_SCANFIFOCLEAR;
#endif // (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE)

  // Modify init structs and initialize
  init.prescale = ADC_PrescaleCalc(NES_PIR_MEASUREMENT_ADC_SAMPLING_FREQUENCY, 0); // Init to max ADC clock for Series 1
  init.timebase = ADC_TimebaseCalc(0);

  ADC_Init(ADC0, &init);
#if (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE)
  ADC_InitSingle(ADC0, &initSingle);
#elif (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  ADC_InitScan(ADC0, &initScan);
#endif // (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE)
}

#if NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE
void nes_sensor_pir_adc0_deinit(void)
{
#if (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
//  CMU_ClockEnable(cmuClock_HFPER, false);
#endif // (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  // Disable ADC0 clock
  CMU_ClockEnable(cmuClock_ADC0, false);
}
#endif // NES_PIR_MEASUREMENT_ENABLE_WORK_WITH_SLEEP_MODE

void nes_sensor_pir_adc0_read(float *adc_values)
{
#if (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  uint32_t i, id;
#endif // (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  uint32_t adc_raw_value;

#if (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE)
#if (NES_PIR_MEASUREMENT_ADC_NUM == 1)
  // Start ADC conversion
  ADC_Start(ADC0, adcStartSingle);

  // Wait for conversion to be complete
  while(!(ADC0->STATUS & _ADC_STATUS_SINGLEDV_MASK));

  // Get ADC result
  adc_raw_value = ADC_DataSingleGet(ADC0);
  adc_values[0] = (float)adc_raw_value / 4096 * 3.30;
#endif
#elif (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SCAN)
  // Start next ADC conversion
  ADC_Start(ADC0, adcStartScan);

  // Get ADC results
  for(i = 0; i < NES_PIR_MEASUREMENT_ADC_NUM; i++)
  {
    // Get data from ADC scan
    adc_raw_value = ADC_DataIdScanGet(ADC0, &id);
    adc_values[i] = (float)adc_raw_value / 4096 * 3.30;
  }
#endif // (NES_PIR_MEASUREMENT_ADC_RUNNING_MODE == NES_PIR_MEASUREMENT_ADC_RUNNING_MODE_SINGLE)
}
#endif // NES_PIR_MEASUREMENT_ENABLE_ADC

#if NES_PIR_MEASUREMENT_ENABLE_PWM
#if (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
void nes_sensor_pir_pwm0_init(void)
{
#if (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
  // Enable clock for TIMER0 module
  CMU_ClockEnable(cmuClock_TIMER0, true);

  // Configure TIMER0 Compare/Capture for output compare
  // Use PWM mode, which sets output on overflow and clears on compare events
  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
  timerCCInit.mode = timerCCModePWM;
  TIMER_InitCC(TIMER0, 0, &timerCCInit);

  TIMER0->ROUTELOC0 |=  NES_PIR_MEASUREMENT_PWM0_TIM0_CC0_LOCATION;
  TIMER0->ROUTEPEN |= TIMER_ROUTEPEN_CC0PEN;

  // Set top value to overflow at the desired PWM_FREQ frequency
  TIMER_TopSet(TIMER0, CMU_ClockFreqGet(cmuClock_TIMER0) / NES_PIR_MEASUREMENT_PWM_FREQUENCY);

  // Initialize and start timer with no prescaling
  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
  TIMER_Init(TIMER0, &timerInit);

  // Initialize duty cycle
  TIMER0->CC[0].CCVB = (uint16_t) (TIMER_TopGet(TIMER0) * NES_PIR_MEASUREMENT_PWM_DUTY_CYCLE / 100);

  // Configure PC10 as output
  // P12 on the WSTK board
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PWM0_PORT, NES_PIR_MEASUREMENT_PWM0_PIN, gpioModePushPull, 0);
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
}
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)

#if (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
void nes_sensor_pir_pwm0_deinit(void)
{
#if (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
  // Disable clock for TIMER0 module
  CMU_ClockEnable(cmuClock_TIMER0, false);

  // Disable PWM mode
  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
  timerCCInit.mode = timerCCModeOff;
  TIMER_InitCC(TIMER0, 0, &timerCCInit);

  // Disable the timer
  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
  timerInit.enable = false;
  TIMER_Init(TIMER0, &timerInit);

  // Disable output GPIO - PC10
  // P12 on the WSTK board
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PWM0_PORT, NES_PIR_MEASUREMENT_PWM0_PIN, gpioModeDisabled, 0);
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
}
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)

#if (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
void nes_sensor_pir_pwm1_init(void)
{
#if (NES_PIR_MEASUREMENT_PWM1_MODE == NES_PIR_MEASUREMENT_PWM1_MODE_TIMER)
  // Enable clock for TIMER1 module
  CMU_ClockEnable(cmuClock_TIMER1, true);

  // Configure TIMER0 Compare/Capture for output compare
  // Use PWM mode, which sets output on overflow and clears on compare events
  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
  timerCCInit.mode = timerCCModePWM;
  TIMER_InitCC(TIMER1, 1, &timerCCInit);

  TIMER1->ROUTELOC0 |=  NES_PIR_MEASUREMENT_PWM1_TIM1_CC1_LOCATION;
  TIMER1->ROUTEPEN |= TIMER_ROUTEPEN_CC1PEN;

  // Set top value to overflow at the desired PWM_FREQ frequency
  TIMER_TopSet(TIMER1, CMU_ClockFreqGet(cmuClock_TIMER1) / NES_PIR_MEASUREMENT_PWM_FREQUENCY);

  // Initialize and start timer with no prescaling
  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
  timerInit.sync = true;
  TIMER_Init(TIMER1, &timerInit);

  // Create phase differences
  TIMER1->CNT = NES_PIR_MEASUREMENT_PWM1_PHASE_SHIFTING_COUNTER;

  // Initialize duty cycle
  TIMER1->CC[1].CCVB = (uint16_t) (TIMER_TopGet(TIMER1) * NES_PIR_MEASUREMENT_PWM_DUTY_CYCLE / 100);

  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PWM1_PORT, NES_PIR_MEASUREMENT_PWM1_PIN, gpioModePushPull, 0);
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
}
#endif // (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)

#if (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
void nes_sensor_pir_pwm1_deinit(void)
{
#if (NES_PIR_MEASUREMENT_PWM1_MODE == NES_PIR_MEASUREMENT_PWM1_MODE_TIMER)
  // Disable clock for TIMER1 module
  CMU_ClockEnable(cmuClock_TIMER1, false);

  // Disable PWM mode
  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
  timerCCInit.mode = timerCCModeOff;
  TIMER_InitCC(TIMER1, 1, &timerCCInit);

  // Disable the timer
  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
  timerInit.enable = false;
  TIMER_Init(TIMER1, &timerInit);

  // Disable output GPIO - PC11
  // P13 on the WSTK board
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_PWM1_PORT, NES_PIR_MEASUREMENT_PWM1_PIN, gpioModeDisabled, 0);
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE == NES_PIR_MEASUREMENT_PWM0_MODE_TIMER)
}
#endif // (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)

void turn_off_pwm_generation(void)
{
#if (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
  nes_sensor_pir_pwm0_deinit();
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
#if (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
  nes_sensor_pir_pwm1_deinit();
#endif // (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)

#if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
  if (!is_in_low_power_mode) {
    // Exit EM1 mode bonding.
    sl_power_manager_remove_em_requirement(SL_POWER_MANAGER_EM1);
    is_in_low_power_mode = true;
  }
#endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
  nes_sensor_pir_peripheral_clear_extend_gpios();
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS

  pwm_is_running = false;
#if NES_PIR_MEASUREMENT_ENABLE_PWM
  sensor_data.pwm_status = false;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM
}

void turn_on_pwm_generation(void)
{
#if (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)
  if (is_in_low_power_mode) {
    // Enter EM1 mode bonding.
    sl_power_manager_add_em_requirement(SL_POWER_MANAGER_EM1);
    is_in_low_power_mode = false;
  }
#endif // (NES_PIR_MEASUREMENT_ENERGY_MODE != NES_PIR_MEASUREMENT_ENERGY_MODE_EM1)

#if (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
  nes_sensor_pir_pwm0_init();
#endif // (NES_PIR_MEASUREMENT_PWM0_MODE != NES_PIR_MEASUREMENT_PWM0_MODE_NONE)
#if (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)
  nes_sensor_pir_pwm1_init();
#endif // (NES_PIR_MEASUREMENT_PWM1_MODE != NES_PIR_MEASUREMENT_PWM1_MODE_NONE)

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
  nes_sensor_pir_peripheral_set_extend_gpios();
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS

  pwm_is_running = true;
#if NES_PIR_MEASUREMENT_ENABLE_PWM
  sensor_data.pwm_status = true;
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM
}
#endif // NES_PIR_MEASUREMENT_ENABLE_PWM

#if NES_PIR_MEASUREMENT_ENABLE_PYD1598
void nes_sensor_pir_pyd1598_init(uint32_t reg_val)
{
  uint8_t i;
  uint8_t next_bit;
  uint32_t reg_mask = 0x1000000;

#if NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO
  serial_in_set_addr = PER_BITSET_MEM_BASE + ((uint32_t)(&GPIO->P[NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PORT].DOUT) - PER_MEM_BASE);
  serial_in_clear_addr = PER_BITCLR_MEM_BASE + ((uint32_t)(&GPIO->P[NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PORT].DOUT) - PER_MEM_BASE);
  serial_in_mask = 1 << NES_PIR_MEASUREMENT_PYD1598_SERIAL_IN_PIN;

  dl_set_addr = PER_BITSET_MEM_BASE + ((uint32_t)(&GPIO->P[NES_PIR_MEASUREMENT_PYD1598_DL_PORT].DOUT) - PER_MEM_BASE);
  dl_clear_addr = PER_BITCLR_MEM_BASE + ((uint32_t)(&GPIO->P[NES_PIR_MEASUREMENT_PYD1598_DL_PORT].DOUT) - PER_MEM_BASE);
  dl_mask = 1 << NES_PIR_MEASUREMENT_PYD1598_DL_PIN;
  dl_read_addr = BITBAND_PER_BASE + \
      (((uint32_t)(&GPIO->P[NES_PIR_MEASUREMENT_PYD1598_DL_PORT].DIN) - PER_MEM_BASE) * (uint32_t)32) + (NES_PIR_MEASUREMENT_PYD1598_DL_PIN * (uint32_t) 4);
#endif // NES_PIR_MEASUREMENT_PYD1598_ENABLE_HIGH_EFFICIENCY_GPIO

  PYD1598_SERIAL_IN_MODE_OUTPUT();
  PYD1598_SERIAL_IN_CLEAR();

  for (i = 0; i < 25; i++)
  {
    next_bit = ((reg_val & reg_mask) != 0);
    reg_mask >>= 1;
    PYD1598_SERIAL_IN_SET();
    switch(next_bit)
    {
      case 0:
        PYD1598_SERIAL_IN_CLEAR();
        break;

      case 1:
        PYD1598_SERIAL_IN_SET();
        break;
    }

    // Wait for 100 microseconds
    sl_udelay_wait(95);
  }

  PYD1598_SERIAL_IN_CLEAR();

  // Wait for 600 microseconds
  sl_udelay_wait(600);
}

int nes_sensor_pir_pyd1598_read(void)
{
  uint8_t i;
  uint16_t ui_bit_mask;
  uint32_t ul_bit_mask;
  int pir_val = 0;
  uint32_t stat_cfg = 0;  // status and cfg registers

  // Configure DL as output
  PYD1598_DL_MODE_OUTPUT();
  // set DL to HIGH
  PYD1598_DL_SET();
  sl_udelay_wait(150);

  //get first 15 bits out-off-range and ADC value
  ui_bit_mask = 0x4000; // set bit mask

  for (i = 0; i<15; i++)
  {
    // create low to high transition
    // Configure DL as output
    PYD1598_DL_MODE_OUTPUT();
    // set DL to LOW
    PYD1598_DL_CLEAR();
    // delay 62.5ns x 4 = 250 ns, should be in 200-2000ns
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    // set DL to HIGH
    PYD1598_DL_SET();
    // delay 62.5ns x 4 = 250 ns, should be in 200-2000ns
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    // Configure DL as input
    PYD1598_DL_MODE_INPUT();
    //Wait for stable low signal, tbd empirically using scope
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    // If DL High set masked bit
    if (PYD1598_DL_GET())
    {
      pir_val |= ui_bit_mask;
    }

    ui_bit_mask >>= 1;
  }

  // get 25bit status and config
  ul_bit_mask = 0x1000000; // Set BitPos

  for (i=0; i < 25; i++)
  {
    // create low to high transition
    // Configure DL as output
    PYD1598_DL_MODE_OUTPUT();
    // Set DL = Low, duration must be > 200 ns (tL)
    PYD1598_DL_CLEAR();
    // number of nop dependant processor speed (200ns min.)
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    // Set DL = High, duration must be > 200 ns (tH)
    PYD1598_DL_SET();
    // number of nop dependant processor speed (200ns min.)
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    // Configure DL as Input
    PYD1598_DL_MODE_INPUT();
    // Wait for stable low signal, tbd empirically using scope
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    // If DL High set masked bit
    if (PYD1598_DL_GET())
    {
      stat_cfg |= ul_bit_mask;
    }
    ul_bit_mask >>= 1;
  }

  // Configure DL as Output
  PYD1598_DL_MODE_OUTPUT();
  // Set DL = Low
  PYD1598_DL_CLEAR();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  // Configure DL as Input
  PYD1598_DL_MODE_INPUT();
  // clear unused bit
  pir_val &= 0x3FFF;

  if (!(stat_cfg & 0x60))
  {
    // ADC source to PIR band pass
    // number in 14bit two's complement
    if(pir_val & 0x2000)
      pir_val -= 0x4000;
  }

  return pir_val;
}
#endif // NES_PIR_MEASUREMENT_ENABLE_PYD1598

#if NES_PIR_MEASUREMENT_ENABLE_SI7021
static void nes_sensor_pir_si7021_temperature_init(void)
{
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_status_t sc;
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sc =
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_si70xx_init(sl_i2cspm_sensor, SI7021_ADDR);
#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to init Si7021 sensor.\n",
                (int)sc);
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
}

#if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE || NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
sl_status_t nes_sensor_pir_si7021_temperature_humidity_read(
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    float *temperature_read
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
    ,
#endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE && NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    float *humidity_read
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    )
{
  sl_status_t sc;
  int32_t temperature = 0;
  uint32_t humidity = 0;

#if defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
  sl_app_assert(temperature_read != NULL,
                "Input temperature pointer is NULL\n");
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  sl_app_assert(humidity_read != NULL,
                "Input humidity pointer is NULL\n");
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
#endif // defined(SL_APP_ASSERT_ENABLE) && SL_APP_ASSERT_ENABLE

  // Measure temperature; units are % and milli-Celsius.
  sc = sl_si70xx_measure_rh_and_temp(sl_i2cspm_sensor,
                                     SI7021_ADDR,
                                     &humidity,
                                     &temperature);
  if (sc) {
#if (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
    nes_sensor_server_log("Warning! Invalid Si7021 reading: %lu %ld\n", humidity, temperature);
#endif // (NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE == NES_PIR_MEASUREMENT_LOCAL_LOGGING_MODE_DEBUG)
  } else {
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
    *temperature_read = (float)temperature / 1000;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE
#if NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
    *humidity_read = (float)humidity / 1000;
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY
  }

  return sc;
}
#endif // (NES_PIR_MEASUREMENT_ENABLE_SI7021_TEMPERATURE || NES_PIR_MEASUREMENT_ENABLE_SI7021_HUMIDITY)
#endif // NES_PIR_MEASUREMENT_ENABLE_SI7021

#if NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
void nes_sensor_pir_peripheral_init_extend_gpios(void)
{
  EXTEND_DRIVE_GPIO_PIR_0_MODE_OUTPUT();
  EXTEND_DRIVE_GPIO_PIR_1_MODE_OUTPUT();
  EXTEND_DRIVE_GPIO_PIR_2_MODE_OUTPUT();
  EXTEND_DRIVE_GPIO_REGULATOR_0_MODE_OUTPUT();
  EXTEND_DRIVE_GPIO_REGULATOR_1_MODE_OUTPUT();
  EXTEND_DRIVE_GPIO_REGULATOR_2_MODE_OUTPUT();
}

static void nes_sensor_pir_peripheral_clear_extend_gpios(void)
{
  EXTEND_DRIVE_GPIO_PIR_0_CLEAR();
  EXTEND_DRIVE_GPIO_PIR_1_CLEAR();
  EXTEND_DRIVE_GPIO_PIR_2_CLEAR();
  EXTEND_DRIVE_GPIO_REGULATOR_0_CLEAR();
  EXTEND_DRIVE_GPIO_REGULATOR_1_CLEAR();
  EXTEND_DRIVE_GPIO_REGULATOR_2_CLEAR();
}

static void nes_sensor_pir_peripheral_set_extend_gpios(void)
{
  EXTEND_DRIVE_GPIO_PIR_0_SET();
  EXTEND_DRIVE_GPIO_PIR_1_SET();
  EXTEND_DRIVE_GPIO_PIR_2_SET();
  EXTEND_DRIVE_GPIO_REGULATOR_0_SET();
  EXTEND_DRIVE_GPIO_REGULATOR_1_SET();
  EXTEND_DRIVE_GPIO_REGULATOR_2_SET();
}
#endif // NES_PIR_MEASUREMENT_ENABLE_EXTEND_DRIVE_GPIOS
