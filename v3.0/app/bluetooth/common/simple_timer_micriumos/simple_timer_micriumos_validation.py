# simple_timer_rtos validation script for OS_CFG_TMR_EN

def validate(project):
  timer_enable = project.config('OS_CFG_TMR_EN')
  if timer_enable is not None:
    if int(timer_enable.value()) == 0:
      project.error('Kernel OS_CFG_TMR_EN config needs to be enabled', project.target_for_defines('OS_CFG_TMR_EN'))
