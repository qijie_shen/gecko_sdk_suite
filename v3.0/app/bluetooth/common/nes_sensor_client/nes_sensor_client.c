/***************************************************************************//**
 * @file
 * @brief NES Sensor Client GATT service
 *******************************************************************************
 * # License
 * <b>Copyright </b>
 *******************************************************************************
 *
 *
 ******************************************************************************/
#include "em_common.h"
#include "sl_bluetooth.h"
#include "gatt_db.h"
#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT
#ifdef SL_CATALOG_CLI_PRESENT
#include "sl_cli.h"
#endif // SL_CATALOG_CLI_PRESENT

#include "nes_sensor_client.h"

#include "app.h"

#include "SensorData.pb-c.h"
#include "sl_iostream.h"
#include "sl_udelay.h"

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#include "em_wdog.h"
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
#include "em_cmu.h"
#include "em_gpio.h"
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION

#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#include <math.h>

#define TEMP_INVALID                  NAN
#define UNIT_INVALID                  ('?')
#define UNIT_CELSIUS                  ('C')
#define UNIT_FAHRENHEIT               ('F')

// Macro to translate the Flags to Celsius (C) or Fahrenheit (F). Flags is the first byte of the
// Temperature Measurement characteristic value according to the Bluetooth SIG
#define translate_flags_to_temperature_unit(flags) (((flags) & 1) ? UNIT_FAHRENHEIT : UNIT_CELSIUS)

typedef struct {
  uint8_t mantissa_l;
  uint8_t mantissa_m;
  int8_t mantissa_h;
  int8_t exponent;
} IEEE_11073_float;
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING

#define RSSI_INVALID                  ((int8_t)0x7F)
#define CONNECTION_HANDLE_INVALID     ((uint8_t)0xFFu)
#define SERVICE_HANDLE_INVALID        ((uint32_t)0xFFFFFFFFu)
#define CHARACTERISTIC_HANDLE_INVALID ((uint16_t)0xFFFFu)
#define TABLE_INDEX_INVALID           ((uint8_t)0xFFu)
#define SERVER_ADDRESS_INVALID        ((uint16_t)0xFFFFu)
#define NWK_STATUS_REASON_INVALID     ((uint16_t)0xFFFFu)

typedef struct {
  uint8_t  connection_handle;
  int8_t   rssi;
  uint16_t server_address;
  uint32_t nessensor_service_handle;
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
  uint16_t thermometer_characteristic_handle;
  float    temperature;
  char     unit;
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  uint16_t nessensor_characteristic_handle[characteristic_discover_done];
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  uint16_t nessensor_characteristic_handle;
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  uint8_t  nessensor_collected_data[NES_SENSOR_CLIENT_RECEIVING_BUFFER_LENGTH_BYTES];
  uint16_t  nessensor_collected_data_len;
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
} nes_sensor_client_conn_properties_t;

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
static const WDOG_Init_TypeDef wdg_init =
{
  .enable = true,             /* Start watchdog when init done */
#if !NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG
  .debugRun = false,          /* WDOG not counting during debug halt */
#else // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG
  .debugRun = true,           /* WDOG not counting during debug halt */
#endif // !NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG
  .em2Run = true,             /* WDOG counting when in EM2 */
  .em3Run = true,             /* WDOG counting when in EM3 */
  .em4Block = false,          /* EM4 can be entered */
  .swoscBlock = false,        /* Do not block disabling LFRCO/LFXO in CMU */
  .lock = false,              /* Do not lock WDOG configuration (if locked, reset needed to unlock) */
  .clkSel = wdogClkSelULFRCO, /* Select 1kHZ WDOG oscillator */
#if (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_1_SECOND)
  .perSel = wdogPeriod_1k,    /* Set the watchdog period to 1025 clock periods (ie ~1 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND)
  .perSel = wdogPeriod_2k,    /* Set the watchdog period to 2049 clock periods (ie ~2 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_4_SECOND)
  .perSel = wdogPeriod_4k,    /* Set the watchdog period to 4097 clock periods (ie ~4 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_8_SECOND)
  .perSel = wdogPeriod_8k,    /* Set the watchdog period to 8193 clock periods (ie ~8 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND)
  .perSel = wdogPeriod_16k,    /* Set the watchdog period to 16385 clock periods (ie ~16 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_32_SECOND)
  .perSel = wdogPeriod_32k,    /* Set the watchdog period to 32769 clock periods (ie ~32 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_64_SECOND)
  .perSel = wdogPeriod_64k,    /* Set the watchdog period to 65537 clock periods (ie ~64 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_128_SECOND)
  .perSel = wdogPeriod_128k,    /* Set the watchdog period to 131073 clock periods (ie ~128 seconds)*/
#elif (NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS == NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_256_SECOND)
  .perSel = wdogPeriod_256k,    /* Set the watchdog period to 262145 clock periods (ie ~256 seconds)*/
#endif
};
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

// Array for holding properties of multiple (parallel) connections
static nes_sensor_client_conn_properties_t conn_properties[SL_BT_CONFIG_MAX_CONNECTIONS];
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
// Health Thermometer service UUID defined by Bluetooth SIG
static const uint8_t nes_sensor_service[2] = { 0x09, 0x18 };
// Temperature Measurement characteristic UUID defined by Bluetooth SIG
static const uint8_t thermo_char[2] = { 0x1c, 0x2a };
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
// NES Sensor service UUID defined by Bluetooth SIG
static const uint8_t nes_sensor_service[2] = { 0x0a, 0x18 };
// NES PIR Measurement characteristic UUID defined by Bluetooth SIG
static const uint8_t nes_sensor_pir_measurement_char[2] = { 0x1d, 0x2a };
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
// NES Sleep PIR Measurement characteristic UUID defined by Bluetooth SIG
static const uint8_t nes_sensor_pir_sleep_measurement_char[2] = { 0x1e, 0x2a };
//
static pir_characteristic_discover_mode_t pir_characteristic_discover_mode;
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
//
static uint8_t nesclient_notification_buffer[NES_CLIENT_NOTIFICATION_BUFFER_LENGTH_BYTES];

static NESClientNotification nes_client_notification_data;
/**************************************************************************//**
 * Initialize NES Sensor Client module.
 *****************************************************************************/
static void nes_sensor_client_init(void);

static uint8_t find_service_in_advertisement(uint8_t *data, uint8_t len);
static uint8_t find_index_by_connection_handle(uint8_t connection);
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
static uint8_t find_index_by_address(uint16_t address);
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
static void add_connection(uint8_t connection, uint16_t address);
// Remove a connection from the connection_properties array
static void remove_connection(uint8_t connection, uint16_t reason);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
static void nes_sensor_client_assert(bool boolean_expression,
                                     uint16_t address,
                                     const char* msg,
                                     int sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE

#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LIST)
static void print_values(nes_sensor_client_conn_properties_t *conn_properties);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LIST)

#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
static float translate_IEEE_11073_temperature_to_float(IEEE_11073_float const *IEEE_11073_value);
// Print RSSI and temperature values
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
static void nes_sensor_client_send_network_status_msg(NwkStatus nwk_status,
                                                      uint16_t server_address,
                                                      uint16_t reason);
#if (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
#if NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA
static size_t nes_sensor_client_unpack_nes_sensor_data_and_print(const uint8_t *buffer, size_t len);
#endif // NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA
#endif // (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
static void nes_sensor_client_upload_data_and_delay(const uint8_t *buffer, uint16_t packet_len);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING

#if NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
static void nes_sensor_client_send_heart_beat_msg(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
#if (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
static size_t nes_sensor_client_unpack_nesclient_notification_data_and_print(const uint8_t *buffer, size_t len);
#endif // (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
static void nes_pir_measurement_init_indication_gpio(void);
static void nes_pir_measurement_indicate_gpio_status(void);
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
/**************************************************************************//**
 * Bluetooth stack event handler.
 *****************************************************************************/
void nes_bt_sensor_client_on_event(sl_bt_msg_t *evt)
{
  sl_status_t sc;
  uint8_t *char_value;
  uint16_t addr_value;
  uint8_t table_index;

  // Handle stack events
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_bt_evt_system_boot_id:
      nes_sensor_client_init();
      break;

    case sl_bt_evt_system_error_id:
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      nes_sensor_client_log("sl_bt_evt_system_error (%u) happens.%c", \
                            evt->data.evt_system_error.reason,
                            NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      break;

    case sl_bt_evt_system_hardware_error_id:
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      nes_sensor_client_log("sl_bt_evt_system_hardware_error (%u) happens.%c", \
                            evt->data.evt_system_hardware_error.status,
                            NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      break;

    case sl_bt_evt_system_soft_timer_id:
      switch (evt->data.evt_system_soft_timer.handle) {
#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
        case NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE:
          WDOG_Feed();
          break;
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
#if NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
        case NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_TIMER_HANDLE:
          nes_sensor_client_send_heart_beat_msg();
          break;
#endif // NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
#if NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
        case NES_PIR_MEASUREMENT_GPIO_INDICATION_TIMER_HANDLE:
          nes_pir_measurement_indicate_gpio_status();
          break;
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
      }
      break;

    // -------------------------------
    // This event is generated when an advertisement packet or a scan response
    // is received from a slave
    case sl_bt_evt_scanner_scan_report_id:
      // Parse advertisement packets
      if (evt->data.evt_scanner_scan_report.packet_type == 0) {
        // If an advertisement is found...
        if (find_service_in_advertisement(&(evt->data.evt_scanner_scan_report.data.data[0]),
                                          evt->data.evt_scanner_scan_report.data.len) != 0) {
          // then stop scanning for a while
          sc = sl_bt_scanner_stop();
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
          nes_sensor_client_assert(sc == SL_STATUS_OK,
                                   SERVER_ADDRESS_INVALID,
                                   "[E: 0x%04x] Failed to stop discovery",
                                   (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
          // and connect to that device
          if (active_connections_num < SL_BT_CONFIG_MAX_CONNECTIONS) {
            sc = sl_bt_connection_open(evt->data.evt_scanner_scan_report.address,
                                       evt->data.evt_scanner_scan_report.address_type,
                                       gap_1m_phy,
                                       NULL);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
            nes_sensor_client_assert(sc == SL_STATUS_OK,
                                     ((uint16_t)(evt->data.evt_scanner_scan_report.address.addr[1] << 8) + evt->data.evt_scanner_scan_report.address.addr[0]),
                                     "[E: 0x%04x @ 0x%04x] Failed to connect",
                                     (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
            conn_state = opening;
          }
        }
      }
      break;

    // -------------------------------
    // This event is generated when a new connection is established
    case sl_bt_evt_connection_opened_id:
#if !NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES
      // nes_sensor_client_log("Connection opened\n");
#endif // !NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES
      // Get last two bytes of sender address
      addr_value = (uint16_t)(evt->data.evt_connection_opened.address.addr[1] << 8) + evt->data.evt_connection_opened.address.addr[0];
      // Add connection to the connection_properties array
      add_connection(evt->data.evt_connection_opened.connection, addr_value);
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      nes_sensor_client_log("Connection opened as %u with 0x%04x.%c",
                            evt->data.evt_connection_opened.connection,
                            addr_value,
                            NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      // Discover NES Sensor service on the slave device
      sc = sl_bt_gatt_discover_primary_services_by_uuid(evt->data.evt_connection_opened.connection,
                                                        sizeof(nes_sensor_service),
                                                        (const uint8_t*)nes_sensor_service);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
      nes_sensor_client_assert(sc == SL_STATUS_OK,
                               addr_value,
                               "[E: 0x%04x @ 0x%04x] Failed to discover primary services",
                               (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
      conn_state = discover_services;
      break;

    // -------------------------------
    // This event is generated when a new service is discovered
    case sl_bt_evt_gatt_service_id:
      table_index = find_index_by_connection_handle(evt->data.evt_gatt_service.connection);
      if (table_index != TABLE_INDEX_INVALID) {
        // Save service handle for future reference
        conn_properties[table_index].nessensor_service_handle = evt->data.evt_gatt_service.service;
      }
      break;

    // -------------------------------
    // This event is generated when a new characteristic is discovered
    case sl_bt_evt_gatt_characteristic_id:
      //nes_sensor_client_log("A new characteristic is discovered as %u\n", evt->data.evt_gatt_characteristic.characteristic);
      table_index = find_index_by_connection_handle(evt->data.evt_gatt_characteristic.connection);
      if (table_index != TABLE_INDEX_INVALID) {
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
        conn_properties[table_index].thermometer_characteristic_handle = evt->data.evt_gatt_characteristic.characteristic;
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        if (pir_characteristic_discover_mode != characteristic_discover_done) {
          // Save characteristic handle for future reference
          conn_properties[table_index].nessensor_characteristic_handle[pir_characteristic_discover_mode] = evt->data.evt_gatt_characteristic.characteristic;
        }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        // Save characteristic handle for future reference
        conn_properties[table_index].nessensor_characteristic_handle = evt->data.evt_gatt_characteristic.characteristic;
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      }
      break;

    // -------------------------------
    // This event is generated for various procedure completions, e.g. when a
    // write procedure is completed, or service discovery is completed
    case sl_bt_evt_gatt_procedure_completed_id:
      table_index = find_index_by_connection_handle(evt->data.evt_gatt_procedure_completed.connection);
      if (table_index == TABLE_INDEX_INVALID) {
        break;
      }
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      // If service discovery finished
      if (conn_state == discover_services && conn_properties[table_index].nessensor_service_handle != SERVICE_HANDLE_INVALID) {
        // Discover thermometer characteristic on the slave device
        sc = sl_bt_gatt_discover_characteristics_by_uuid(evt->data.evt_gatt_procedure_completed.connection,
                                                         conn_properties[table_index].nessensor_service_handle,
                                                         sizeof(thermo_char),
                                                         (const uint8_t*)thermo_char);
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x] Failed to discover characteristics\n",
                                 (int)sc);
        conn_state = discover_characteristics;
        break;
      }
      // If characteristic discovery finished
      if (conn_state == discover_characteristics && conn_properties[table_index].thermometer_characteristic_handle != CHARACTERISTIC_HANDLE_INVALID) {
#if 0   // Invalid state error
        // stop discovering
        sc = sl_bt_scanner_stop();
        conn_properties[table_index].server_address,(sc == SL_STATUS_OK,
                                                    SERVER_ADDRESS_INVALID,
                                                    "[E: 0x%04x] Failed to stop discovery #2\n",
                                                    (int)sc);
#endif
        // enable indications
        sc = sl_bt_gatt_set_characteristic_notification(evt->data.evt_gatt_procedure_completed.connection,
                                                        conn_properties[table_index].thermometer_characteristic_handle,
                                                        gatt_indication);
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x] Failed to set characteristic notification\n",
                                 (int)sc);
        active_connections_num++;
        conn_state = enable_indication;
        break;
      }
#else
      // If service discovery finished
      if (conn_state == discover_services && conn_properties[table_index].nessensor_service_handle != SERVICE_HANDLE_INVALID) {
        // Discover nessensor characteristic on the slave device
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        switch (pir_characteristic_discover_mode) {
          case characteristic_discover_pir_measurement:
            sc = sl_bt_gatt_discover_characteristics_by_uuid(evt->data.evt_gatt_procedure_completed.connection,
                                                             conn_properties[table_index].nessensor_service_handle,
                                                             sizeof(nes_sensor_pir_measurement_char),
                                                             (const uint8_t*)nes_sensor_pir_measurement_char);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
            nes_sensor_client_assert(sc == SL_STATUS_OK,
                                     conn_properties[table_index].server_address,
                                     "[E: 0x%04x @ 0x%04x] Failed to discover nes_sensor_pir_measurement characteristics",
                                     (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
            break;
          case characteristic_discover_pir_sleep_measurement:
            sc = sl_bt_gatt_discover_characteristics_by_uuid(evt->data.evt_gatt_procedure_completed.connection,
                                                             conn_properties[table_index].nessensor_service_handle,
                                                             sizeof(nes_sensor_pir_sleep_measurement_char),
                                                             (const uint8_t*)nes_sensor_pir_sleep_measurement_char);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
            nes_sensor_client_assert(sc == SL_STATUS_OK,
                                     conn_properties[table_index].server_address,
                                     "[E: 0x%04x @ 0x%04x] Failed to discover nes_sensor_pir_sleep_measurement characteristics",
                                     (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
            break;
          default:
            break;
        }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        sc = sl_bt_gatt_discover_characteristics_by_uuid(evt->data.evt_gatt_procedure_completed.connection,
                                                         conn_properties[table_index].nessensor_service_handle,
                                                         sizeof(nes_sensor_pir_measurement_char),
                                                         (const uint8_t*)nes_sensor_pir_measurement_char);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x @ 0x%04x] Failed to discover nes_sensor_pir_measurement characteristics",
                                 (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        conn_state = discover_characteristics;
        break;
      }
      // If characteristic discovery finished
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
      if (conn_state == discover_characteristics && conn_properties[table_index].nessensor_characteristic_handle[pir_characteristic_discover_mode] != CHARACTERISTIC_HANDLE_INVALID) {
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
        nes_sensor_client_log("A characteristic discovery is finished with characteristic %u, connection %u for the node 0x%04x.%c",
                   conn_properties[table_index].nessensor_characteristic_handle[pir_characteristic_discover_mode],
                   evt->data.evt_gatt_procedure_completed.connection,
                   conn_properties[table_index].server_address,
                   NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
        // enable indications
        sc = sl_bt_gatt_set_characteristic_notification(evt->data.evt_gatt_procedure_completed.connection,
                                                        conn_properties[table_index].nessensor_characteristic_handle[pir_characteristic_discover_mode],
                                                        gatt_indication);
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
        active_connections_num++;
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x @ 0x%04x] Failed to set characteristic notification",
                                 (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
        pir_characteristic_discover_mode++;
        if (pir_characteristic_discover_mode == characteristic_discover_done) {
          conn_state = enable_indication;
          pir_characteristic_discover_mode = characteristic_discover_pir_measurement;
        } else {
          conn_state = discover_services;
        }
        break;
      }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
      // If characteristic discovery finished
      if (conn_state == discover_characteristics && conn_properties[table_index].nessensor_characteristic_handle != CHARACTERISTIC_HANDLE_INVALID) {
        nes_sensor_client_send_network_status_msg(NWK_STATUS__nwk_char_discovery_done,
                                                  conn_properties[table_index].server_address,
                                                  NWK_STATUS_REASON_INVALID);

        // enable indications
        sc = sl_bt_gatt_set_characteristic_notification(evt->data.evt_gatt_procedure_completed.connection,
                                                        conn_properties[table_index].nessensor_characteristic_handle,
                                                        gatt_indication);
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
        active_connections_num++;
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x @ 0x%04x] Failed to set characteristic notification",
                                 (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
        conn_state = enable_indication;
        break;
      }
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if !NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES
      // If indication enable process finished
      if (conn_state == enable_indication) {
        // and we can connect to more devices
        if (active_connections_num < SL_BT_CONFIG_MAX_CONNECTIONS) {
          // start scanning again to find new devices
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
          nes_sensor_client_log("A node has finished the indication enable process. Start scanning again to find new devices\n");
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
          sc = sl_bt_scanner_start(gap_1m_phy, scanner_discover_generic);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
          nes_sensor_client_assert(sc == SL_STATUS_OK,
                                   SERVER_ADDRESS_INVALID,
                                   "[E: 0x%04x] Failed to start discovery #2\n",
                                   (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
          conn_state = scanning;
        } else {
          conn_state = running;
        }
        break;
      }
#endif // !NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES
      break;

    // -------------------------------
    // This event is generated when a connection is dropped
    case sl_bt_evt_connection_closed_id:
      // remove connection from active connections
      remove_connection(evt->data.evt_connection_closed.connection,
                        evt->data.evt_connection_closed.reason);
#if !NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES
      // nes_sensor_client_log("Connection closed\n");
      if (conn_state != scanning) {
        // start scanning again to find new devices
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
        nes_sensor_client_log("A node is closed. Start scanning again to find new devices.%c",
                              NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
        sc = sl_bt_scanner_start(gap_1m_phy, scanner_discover_generic);
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 SERVER_ADDRESS_INVALID,
                                 "[E: 0x%04x] Failed to start discovery #3\n",
                                 (int)sc);
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
        conn_state = scanning;
      }
#endif // !NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES
      break;

    // -------------------------------
    // This event is generated when a characteristic value was received e.g. an indication
    case sl_bt_evt_gatt_characteristic_value_id:
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      if (evt->data.evt_gatt_characteristic_value.value.len >= 5) {
        char_value = &(evt->data.evt_gatt_characteristic_value.value.data[0]);
        table_index = find_index_by_connection_handle(evt->data.evt_gatt_characteristic_value.connection);
        if (table_index != TABLE_INDEX_INVALID) {
          conn_properties[table_index].temperature = translate_IEEE_11073_temperature_to_float((IEEE_11073_float *)(char_value + 1));
          conn_properties[table_index].unit = translate_flags_to_temperature_unit(char_value[0]);
        }
      } else {
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
        nes_sensor_client_log("Characteristic value too short: %d%c", evt->data.evt_gatt_characteristic_value.value.len,
                              NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      }
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      if (evt->data.evt_gatt_characteristic_value.value.len > 0) {
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        switch (evt->data.evt_gatt_characteristic_value.characteristic) {
          case GATTDB_PIR_MEASUREMENT:
          case GATTDB_PIR_SLEEP_MEASUREMENT:
            char_value = &(evt->data.evt_gatt_characteristic_value.value.data[0]);
            table_index = find_index_by_connection_handle(evt->data.evt_gatt_characteristic_value.connection);
            conn_properties[table_index].nessensor_collected_data_len = evt->data.evt_gatt_characteristic_value.value.len;
            if (table_index != TABLE_INDEX_INVALID) {
              // Store data length into a byte
              conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_BUFFER_LENGTH_POSITION] = conn_properties[table_index].nessensor_collected_data_len;
              // Store pure binary protobuf data
              memcpy(conn_properties[table_index].nessensor_collected_data+DATA_EXCHANGE_NODE_BUFFER_DATA_START_POSITION, char_value, sizeof(uint8_t) * conn_properties[table_index].nessensor_collected_data_len);
              // Mark the end indication symbol
              conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_BUFFER_DATA_START_POSITION+conn_properties[table_index].nessensor_collected_data_len] = DATA_EXCHANGE_END_CHARACTER;
            }
            break;
        }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        char_value = &(evt->data.evt_gatt_characteristic_value.value.data[0]);
        table_index = find_index_by_connection_handle(evt->data.evt_gatt_characteristic_value.connection);
        conn_properties[table_index].nessensor_collected_data_len = evt->data.evt_gatt_characteristic_value.value.len;
        if (table_index != TABLE_INDEX_INVALID) {
          // Store data length into a byte
          conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_BUFFER_LENGTH_POSITION] = conn_properties[table_index].nessensor_collected_data_len;
          // Store pure binary protobuf data
          memcpy(conn_properties[table_index].nessensor_collected_data+DATA_EXCHANGE_NODE_BUFFER_DATA_START_POSITION, char_value, sizeof(uint8_t) * conn_properties[table_index].nessensor_collected_data_len);
          // Mark the end indication symbol
          conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_BUFFER_DATA_START_POSITION+conn_properties[table_index].nessensor_collected_data_len] = DATA_EXCHANGE_END_CHARACTER;
        }
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
        // Send confirmation for the indication
        sc = sl_bt_gatt_send_characteristic_confirmation(evt->data.evt_gatt_characteristic_value.connection);
  #if NES_PIR_MEASUREMENT_ASSERT_ENABLE
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x @ 0x%04x] Failed to send characteristic confirmation",
                                 (int)sc);
  #endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
        // Trigger RSSI measurement on the connection
        sc = sl_bt_connection_get_rssi(evt->data.evt_gatt_characteristic_value.connection);
  #if NES_PIR_MEASUREMENT_ASSERT_ENABLE
        nes_sensor_client_assert(sc == SL_STATUS_OK,
                                 conn_properties[table_index].server_address,
                                 "[E: 0x%04x @ 0x%04x] Failed to get RSSI",
                                 (int)sc);
  #endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
      } else {
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
        nes_sensor_client_log("Characteristic value too short: %d%c",
                              evt->data.evt_gatt_characteristic_value.value.len,
                              NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      }
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      break;

    // -------------------------------
    // This event is generated when RSSI value was measured
    case sl_bt_evt_connection_rssi_id:
      table_index = find_index_by_connection_handle(evt->data.evt_connection_rssi.connection);
      if (table_index != TABLE_INDEX_INVALID) {
        conn_properties[table_index].rssi = evt->data.evt_connection_rssi.rssi;
      }

#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LIST)
      // Print the values
      print_values(conn_properties);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LIST)
#if !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      sprintf((char*)conn_properties[table_index].nessensor_collected_data+DATA_EXCHANGE_NODE_ADDRESS_START_POSITION, "%04x", conn_properties[table_index].server_address);
      conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_ADDRESS_END_POSITION] = DATA_EXCHANGE_SPLIT_CHARACTER;
      sprintf((char*)conn_properties[table_index].nessensor_collected_data+DATA_EXCHANGE_NODE_RSSI_START_POSITION, "%3d", conn_properties[table_index].rssi);
      conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_RSSI_END_POSITION] = DATA_EXCHANGE_SPLIT_CHARACTER;
#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      nes_sensor_client_log("Current packet is from 0x%04x with %3d.%c",
                            conn_properties[table_index].server_address,
                            conn_properties[table_index].rssi,
                            NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW)
      // Convert this length to of the total number of bytes to send
      conn_properties[table_index].nessensor_collected_data_len = conn_properties[table_index].nessensor_collected_data_len + DATA_EXCHANGE_NODE_FIXED_DATA_LENGTH_BYTES;
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
      nes_sensor_client_upload_data_and_delay(conn_properties[table_index].nessensor_collected_data, conn_properties[table_index].nessensor_collected_data_len);
      // sl_iostream_write(SL_IOSTREAM_STDOUT, conn_properties[table_index].nessensor_collected_data, conn_properties[table_index].nessensor_collected_data_len);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
#if NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT
#if NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA
      nes_sensor_client_unpack_nes_sensor_data_and_print(&conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_BUFFER_DATA_START_POSITION+1],
                                                                    (conn_properties[table_index].nessensor_collected_data[DATA_EXCHANGE_NODE_BUFFER_LENGTH_POSITION] - 1));
#endif // NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA
#endif // NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
      break;

    default:
      break;
  }
}

// -----------------------------------------------------------------------------
// Private function definitions

#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
static void nes_sensor_client_assert(bool boolean_expression,
                                     uint16_t address,
                                     const char* msg,
                                     int sc)
{
  size_t msg_len;
  size_t packet_len;

  if (!(boolean_expression)) {
    nes__client_notification__init(&nes_client_notification_data);
    nes_client_notification_data.notification_type = NOTIFICATION_TYPE__Assert;
    nes_client_notification_data.has_nwk_status = false;
    nes_client_notification_data.has_node_address = false;
    msg_len = strlen(msg) + 1;
    nes_client_notification_data.extended_msg = (char*)malloc(sizeof(char) * msg_len);
    if (nes_client_notification_data.extended_msg) {
      memset(nes_client_notification_data.extended_msg, '\0', sizeof(char) * msg_len);
      if (address == SERVER_ADDRESS_INVALID) {
        sprintf(nes_client_notification_data.extended_msg, msg, sc);
      } else {
        sprintf(nes_client_notification_data.extended_msg, msg, sc, address);
      }
    }

    // pack and send
    packet_len = nes__client_notification__pack(&nes_client_notification_data,
                                                &nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION]);
    if (packet_len) {
      nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION] = packet_len;
      packet_len += CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION;
      // Add '#' to the tail
      nesclient_notification_buffer[packet_len] = CLIENT_SPECIFIC_BUFFER_END_CHARACTER;
      packet_len += 1;
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
      nes_sensor_client_upload_data_and_delay(nesclient_notification_buffer, packet_len);
      //sl_iostream_write(SL_IOSTREAM_STDOUT, nesclient_notification_buffer, packet_len);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
#if (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
      nes_sensor_client_unpack_nesclient_notification_data_and_print(&nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION],
                                                                     nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION]);
#endif // (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
    }

    if (nes_client_notification_data.extended_msg) {
      free(nes_client_notification_data.extended_msg);
      nes_client_notification_data.extended_msg = NULL;
    }
  }
}
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE

static void nes_sensor_client_init(void)
{
  uint8_t i;
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  uint8_t j;
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  active_connections_num = 0;

  for (i = 0; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
    conn_properties[i].connection_handle = CONNECTION_HANDLE_INVALID;
    conn_properties[i].nessensor_service_handle = SERVICE_HANDLE_INVALID;
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
    conn_properties[i].thermometer_characteristic_handle = CHARACTERISTIC_HANDLE_INVALID;
    conn_properties[i].temperature = TEMP_INVALID;
    conn_properties[i].unit = UNIT_INVALID;
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    for (j = 0; j < characteristic_discover_done; j++) {
      conn_properties[i].nessensor_characteristic_handle[j] = CHARACTERISTIC_HANDLE_INVALID;
    }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    conn_properties[i].nessensor_characteristic_handle = CHARACTERISTIC_HANDLE_INVALID;
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    memset(conn_properties[i].nessensor_collected_data, '\0', sizeof(uint8_t) * NES_SENSOR_CLIENT_RECEIVING_BUFFER_LENGTH_BYTES);
    // Init basic buffer structure
    conn_properties[i].nessensor_collected_data[0] = DATA_EXCHANGE_HEAD_CHARACTER;
    conn_properties[i].nessensor_collected_data[1] = '0';
    conn_properties[i].nessensor_collected_data[2] = 'x';

    conn_properties[i].nessensor_collected_data_len = 0;
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
    conn_properties[i].rssi = RSSI_INVALID;
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
    conn_properties[i].server_address = SERVER_ADDRESS_INVALID;
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  }

  nesclient_notification_buffer[0] = CLIENT_SPECIFIC_HEAD_CHARACTER;
  memset(&nesclient_notification_buffer[1], '\0', sizeof(uint8_t) * NES_CLIENT_NOTIFICATION_BUFFER_LENGTH_BYTES);

  nes__client_notification__init(&nes_client_notification_data);

#if NES_PIR_MEASUREMENT_ENABLE_WATCHDOG
  // Initialize watchdog timer
  WDOG_Init(&wdg_init);
  sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS),
                              NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE,
                              0);
#endif // NES_PIR_MEASUREMENT_ENABLE_WATCHDOG

#if NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
  sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_PERIOD_SECONDS),
                              NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_TIMER_HANDLE,
                              0);
#endif // NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
  nes_pir_measurement_init_indication_gpio();
  sl_bt_system_set_soft_timer((TICKS_TO_DELAY_ONE_SECOND * NES_PIR_MEASUREMENT_GPIO_INDICATION_PERIOD_SECONDS),
                              NES_PIR_MEASUREMENT_GPIO_INDICATION_TIMER_HANDLE,
                              0);
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
}

// Parse advertisements looking for advertised NES Sensor service
static uint8_t find_service_in_advertisement(uint8_t *data, uint8_t len)
{
  uint8_t ad_field_length;
  uint8_t ad_field_type;
  uint8_t i = 0;
  // Parse advertisement packet
  while (i < len) {
    ad_field_length = data[i];
    ad_field_type = data[i + 1];
    // Partial ($02) or complete ($03) list of 16-bit UUIDs
    if (ad_field_type == 0x02 || ad_field_type == 0x03) {
      // compare UUID to NES Sensor service UUID
      if (memcmp(&data[i + 2], nes_sensor_service, 2) == 0) {
        return 1;
      }
    }
    // advance to the next AD struct
    i = i + ad_field_length + 1;
  }
  return 0;
}

// Find the index of a given connection in the connection_properties array
static uint8_t find_index_by_connection_handle(uint8_t connection)
{
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  for (uint8_t i = 0; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
#else // !NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  for (uint8_t i = 0; i < active_connections_num; i++) {
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
    if (conn_properties[i].connection_handle == connection) {
      return i;
    }
  }
  return TABLE_INDEX_INVALID;
}

#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
// Find the index of a given address in the connection_properties array
static uint8_t find_index_by_address(uint16_t address)
{
  for (uint8_t i = 0; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
    if (conn_properties[i].server_address == address) {
      return i;
    }
  }
  return TABLE_INDEX_INVALID;
}
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING

// Add a new connection to the connection_properties array
static void add_connection(uint8_t connection, uint16_t address)
{
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  uint8_t i;
  uint8_t table_index = find_index_by_address(address);

  if (table_index == TABLE_INDEX_INVALID) {
    // newly added, allocate for it
    for (i = 0; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
      if (conn_properties[i].server_address == SERVER_ADDRESS_INVALID) {
        break;
      }
    }

    if (i == SL_BT_CONFIG_MAX_CONNECTIONS) {
      // exceeding
    } else {
      conn_properties[i].connection_handle = connection;
      conn_properties[i].server_address    = address;
    }
  } else {
    // address existing, update
    conn_properties[table_index].connection_handle = connection;
  }
#else // !NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  conn_properties[active_connections_num].connection_handle = connection;
  conn_properties[active_connections_num].server_address    = address;
  active_connections_num++;
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
#if !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
  nes_sensor_client_send_network_status_msg(NWK_STATUS__nwk_on,
                                            address,
                                            NWK_STATUS_REASON_INVALID);
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
}

// Remove a connection from the connection_properties array
static void remove_connection(uint8_t connection, uint16_t reason)
{
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
  (void)reason;
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if !NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  uint8_t i;
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
#if !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
  uint8_t j;
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
#endif // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
  uint8_t table_index = find_index_by_connection_handle(connection);

#if !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
  nes_sensor_client_send_network_status_msg(NWK_STATUS__nwk_off,
                                            conn_properties[table_index].server_address,
                                            reason);
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
  if (active_connections_num > 0) {
    active_connections_num--;
  }
#if NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  if (table_index != TABLE_INDEX_INVALID) {
    conn_properties[table_index].connection_handle = CONNECTION_HANDLE_INVALID;
    conn_properties[table_index].nessensor_service_handle = SERVICE_HANDLE_INVALID;
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
    conn_properties[table_index].thermometer_characteristic_handle = CHARACTERISTIC_HANDLE_INVALID;
    conn_properties[table_index].temperature = TEMP_INVALID;
    conn_properties[table_index].unit = UNIT_INVALID;
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    for (j = 0; j < characteristic_discover_done; j++) {
      conn_properties[table_index].nessensor_characteristic_handle[j] = CHARACTERISTIC_HANDLE_INVALID;
    }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    conn_properties[table_index].nessensor_characteristic_handle = CHARACTERISTIC_HANDLE_INVALID;
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
    conn_properties[table_index].rssi = RSSI_INVALID;
  }
#else // !NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
  // Shift entries after the removed connection toward 0 index
  for (i = table_index; i < active_connections_num; i++) {
    conn_properties[i] = conn_properties[i + 1];
  }
  // Clear the slots we've just removed so no junk values appear
  for (i = active_connections_num; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
    conn_properties[i].connection_handle = CONNECTION_HANDLE_INVALID;
    conn_properties[i].nessensor_service_handle = SERVICE_HANDLE_INVALID;
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
    conn_properties[i].thermometer_characteristic_handle = CHARACTERISTIC_HANDLE_INVALID;
    conn_properties[i].temperature = TEMP_INVALID;
    conn_properties[i].unit = UNIT_INVALID;
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    for (j = 0; j < characteristic_discover_done; j++) {
      conn_properties[i].nessensor_characteristic_handle[j] = CHARACTERISTIC_HANDLE_INVALID;
    }
#else // !NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    conn_properties[i].nessensor_characteristic_handle = CHARACTERISTIC_HANDLE_INVALID;
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
    conn_properties[i].rssi = RSSI_INVALID;
  }
#endif // NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING
}

#if (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LIST)
static void print_values(nes_sensor_client_conn_properties_t *conn_properties)
{
  static bool print_header = true;
  uint8_t i;

  if (true == print_header) {
    print_header = false;
    for (i = 0u; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
      nes_sensor_client_log("ADDR      RSSI  |");
    }
    nes_sensor_client_log("%c", NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
  }
  for (i = 0u; i < SL_BT_CONFIG_MAX_CONNECTIONS; i++) {
#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
    if ((TEMP_INVALID != conn_properties[i].temperature) && (RSSI_INVALID != conn_properties[i].rssi) ) {
      nes_sensor_client_log("%04x ", conn_properties[i].server_address);
      if (conn_properties[i].temperature < 10.00) {
        conn_properties[i].temperature = 10.00;
      }
      nes_sensor_client_log("%6.2f", conn_properties[i].temperature);
      nes_sensor_client_log("%c ", conn_properties[i].unit);
#else
    if (RSSI_INVALID != conn_properties[i].rssi) {
      nes_sensor_client_log("%04x ", conn_properties[i].server_address);
#endif
      nes_sensor_client_log("% 3d", conn_properties[i].rssi);
      nes_sensor_client_log("dBm|");
    } else {
      nes_sensor_client_log("---- --------|");
    }
  }
  nes_sensor_client_log("   active %u%c", active_connections_num, NES_PIR_MEASUREMENT_LOGGING_END_CHAR);
}
#endif // (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_LIST)

#if NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
// Translate a IEEE-11073 Temperature Value to a float Value
static float translate_IEEE_11073_temperature_to_float(IEEE_11073_float const *IEEE_11073_value)
{
  int32_t mantissa = 0;
  uint8_t mantissa_l;
  uint8_t mantissa_m;
  int8_t mantissa_h;
  int8_t exponent;

  // Wrong Argument: NULL pointer is passed
  if ( !IEEE_11073_value ) {
    return NAN;
  }

  // Caching Fields
  mantissa_l = IEEE_11073_value->mantissa_l;
  mantissa_m = IEEE_11073_value->mantissa_m;
  mantissa_h = IEEE_11073_value->mantissa_h;
  exponent =  IEEE_11073_value->exponent;

  // IEEE-11073 Standard NaN Value Passed
  if ((mantissa_l == 0xFF) && (mantissa_m == 0xFF) && (mantissa_h == 0x7F) && (exponent == 0x00)) {
    return NAN;
  }

  // Converting a 24bit Signed Value to a 32bit Signed Value
  mantissa |= mantissa_h;
  mantissa <<= 8;
  mantissa |= mantissa_m;
  mantissa <<= 8;
  mantissa |= mantissa_l;
  mantissa <<= 8;
  mantissa >>= 8;

  return ((float)mantissa) * pow(10.0f, (float)exponent);
}
#else // !NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING
static void nes_sensor_client_send_network_status_msg(NwkStatus nwk_status,
                                                      uint16_t server_address,
                                                      uint16_t reason)
{
  size_t packet_len;

  // Send disconnection notifications
  nes__client_notification__init(&nes_client_notification_data);
  nes_client_notification_data.notification_type = NOTIFICATION_TYPE__Log;
  nes_client_notification_data.has_node_address = true;
  nes_client_notification_data.node_address = server_address;
  nes_client_notification_data.has_nwk_status = true;
  nes_client_notification_data.nwk_status = nwk_status;
  if (reason != NWK_STATUS_REASON_INVALID) {
    nes_client_notification_data.has_nwk_status_reason = true;
    nes_client_notification_data.nwk_status_reason = reason;
  } else {
    nes_client_notification_data.has_nwk_status_reason = false;
  }

  // pack and send
  packet_len = nes__client_notification__pack(&nes_client_notification_data,
                                              &nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION]);
  if (packet_len) {
    nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION] = packet_len;
    packet_len += CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION;
    // Add '#' to the tail
    nesclient_notification_buffer[packet_len] = CLIENT_SPECIFIC_BUFFER_END_CHARACTER;
    packet_len += 1;
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
    nes_sensor_client_upload_data_and_delay(nesclient_notification_buffer, packet_len);
    //sl_iostream_write(SL_IOSTREAM_STDOUT, nesclient_notification_buffer, packet_len);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
#if (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
    nes_sensor_client_unpack_nesclient_notification_data_and_print(&nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION],
                                                                   nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION]);
#endif // (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
  }
}

#if (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
#if NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA
static size_t nes_sensor_client_unpack_nes_sensor_data_and_print(const uint8_t *buffer, size_t len)
{
  NESSensorData *sensor_data = NULL;
  uint8_t i, j;

#if NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
  nes_sensor_client_log("Indication head char is %c. ", buffer[0]);
  len--;
  sensor_data = nes__sensor_data__unpack(NULL, len, &buffer[1]);
#else // !NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
  sensor_data = nes__sensor_data__unpack(NULL, len, buffer);
#endif // NES_PIR_MEASUREMENT_ENABLE_INDICATION_HEAD_CHAR
  if(!sensor_data) {
      nes_sensor_client_log("nes__sensor_data__unpack is fail!!!\n");
    return 1;
  }
#if 0
  if (sensor_data->sequence_status->packet_loss) {
    nes_sensor_client_log("Indication has packets lost. ");
    nes_sensor_client_log("Blocked sequence is %u and current sequence is %u.\n",
               sensor_data->sequence_status->blocked_sequence,
               sensor_data->sequence_status->current_sequence);
  } else {
    nes_sensor_client_log("Indication has no packets lost. ");
    nes_sensor_client_log("Current sequence is %u.\n",
               sensor_data->sequence_status->current_sequence);
  }
#endif

  switch(sensor_data->type) {
    case INDICATION_TYPE__Normal_Start:
      nes_sensor_client_log("Normal start ");
      break;
    case INDICATION_TYPE__Normal_Unconcerned:
      nes_sensor_client_log("Normal unconcerned ");
      break;
    case INDICATION_TYPE__Normal_End:
      nes_sensor_client_log("Normal end ");
      break;
    default:
      break;
  }

  switch(sensor_data->type) {
    case INDICATION_TYPE__Normal_Start:
    case INDICATION_TYPE__Normal_End:
    case INDICATION_TYPE__Normal_Unconcerned:
      //nes_sensor_client_log("Normal ");

      if (sensor_data->has_pwm_status) {
          nes_sensor_client_log("PWM is on: ");
        if (sensor_data->pwm_status) {
          nes_sensor_client_log("1| ");
        } else {
          nes_sensor_client_log("0| ");
        }
      } else {
        nes_sensor_client_log("PWM is off| ");
      }

      // temp
      if (sensor_data->has_temp) {
        nes_sensor_client_log("temp is on: %f| ", sensor_data->temp);
      } else {
        nes_sensor_client_log("temp is off| ");
      }

      // humidity
      if (sensor_data->has_humidity) {
        nes_sensor_client_log("humidity is on: %f| ", sensor_data->humidity);
      } else {
        nes_sensor_client_log("humidity is off| ");
      }

      nes_sensor_client_log("\n");

      if (sensor_data->n_element) {
        for (i = 0; i< sensor_data->n_element; i++) {

          // ADC
          if (sensor_data->element[i]->n_adc_values) {
            nes_sensor_client_log("ADC is on: ");

            for (j=0; j<sensor_data->element[i]->n_adc_values; j++) {
              nes_sensor_client_log("ADC[%d] = %f |", j, sensor_data->element[i]->adc_values[j]);
            }
            nes_sensor_client_log(" ");
          } else {
            nes_sensor_client_log("ADC is off| ");
          }

          // gpio
          if (sensor_data->element[i]->has_gpio) {
            nes_sensor_client_log("GPIO is on: GPIO is %d\n", (sensor_data->element[i]->gpio == true) ? (1):(0));
          } else {
            nes_sensor_client_log("gpio is off\n");
          }
        }
      }
      break;
    case INDICATION_TYPE__Interrupt:
      nes_sensor_client_log("Interrupt\n");
      break;
    default:
      break;
  }

  nes_sensor_client_log("\n");
  nes__sensor_data__free_unpacked(sensor_data, NULL);

  return 0;
}
#endif // NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA
#endif // (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))

#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
static void nes_sensor_client_upload_data_and_delay(const uint8_t *buffer, uint16_t packet_len)
{
  sl_iostream_write(SL_IOSTREAM_STDOUT, buffer, packet_len);
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_THEN_WAIT_US
  sl_udelay_wait(NES_PIR_MEASUREMENT_UPLOAD_DATA_THEN_WAIT_US);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_THEN_WAIT_US
}
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
#endif // NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING

#if NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
static void nes_sensor_client_send_heart_beat_msg(void)
{
  size_t packet_len;

  // Send disconnection notifications
  nes__client_notification__init(&nes_client_notification_data);
  nes_client_notification_data.notification_type = NOTIFICATION_TYPE__HeartBeat;
  nes_client_notification_data.has_node_address = false;
  nes_client_notification_data.has_nwk_status = false;

  // pack and send
  packet_len = nes__client_notification__pack(&nes_client_notification_data,
                                              &nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION]);
  if (packet_len) {
    nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION] = packet_len;
    packet_len += CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION;
    // Add '#' to the tail
    nesclient_notification_buffer[packet_len] = CLIENT_SPECIFIC_BUFFER_END_CHARACTER;
    packet_len += 1;
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
    nes_sensor_client_upload_data_and_delay(nesclient_notification_buffer, packet_len);
    //sl_iostream_write(SL_IOSTREAM_STDOUT, nesclient_notification_buffer, packet_len);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
#if NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT
    nes_sensor_client_unpack_nesclient_notification_data_and_print(&nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION],
                                                                   nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION]);
#endif // NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT
  }
}
#endif // NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION

#if (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))
static size_t nes_sensor_client_unpack_nesclient_notification_data_and_print(const uint8_t *buffer, size_t len)
{
  NESClientNotification *client_data = NULL;

  client_data = nes__client_notification__unpack(NULL, len, buffer);

  if(!client_data) {
      nes_sensor_client_log("nes__client_notification__unpack is fail!!!\n");
    return 1;
  }

  switch(client_data->notification_type) {
    case NOTIFICATION_TYPE__Log:
      nes_sensor_client_log("This is a log message. ");
      if (client_data->has_node_address) {
          nes_sensor_client_log("The related node is 0x%04x. ", client_data->node_address);
        if (client_data->has_nwk_status) {
          switch(client_data->nwk_status) {
            case NWK_STATUS__nwk_on:
              nes_sensor_client_log("The node is in on state.\n");
              break;
            case NWK_STATUS__nwk_off:
              nes_sensor_client_log("The node is in off state.\n");
              break;
            case NWK_STATUS__nwk_char_discovery_done:
              nes_sensor_client_log("The node is done with char discovery.\n");
              break;
            default:
              break;
          }
        }
      }
      break;
#if NES_PIR_MEASUREMENT_ASSERT_ENABLE
    case NOTIFICATION_TYPE__Assert:
      nes_sensor_client_log("This is an assertion message.\n");
      if (client_data->extended_msg) {
        nes_sensor_client_log("The assertion is %s.\n", client_data->extended_msg);
      }
      break;
#endif // NES_PIR_MEASUREMENT_ASSERT_ENABLE
#if NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
    case NOTIFICATION_TYPE__HeartBeat:
      nes_sensor_client_log("This is a heart-beat message.\n");
      break;
#endif // NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION
    default:
      break;
  }

  nes__client_notification__free_unpacked(client_data, NULL);

  return 0;
}
#endif // (NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT && (NES_PIR_MEASUREMENT_LOGGING_MODE == NES_PIR_MEASUREMENT_LOGGING_MODE_RAW))

#if NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
static void nes_pir_measurement_init_indication_gpio(void)
{
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, \
                    NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN, gpioModeInput, 0);
}

static void nes_pir_measurement_indicate_gpio_status(void)
{
  size_t packet_len;
  // Get the GPIO instant value
  // Pack and send
  nes__client_notification__init(&nes_client_notification_data);
  nes_client_notification_data.notification_type = NOTIFICATION_TYPE__Sensor;
  nes_client_notification_data.has_gpio_status = true;
  nes_client_notification_data.gpio_status = GPIO_PinInGet(NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT, \
                                                           NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN);
  // pack and send
  packet_len = nes__client_notification__pack(&nes_client_notification_data,
                                              &nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION]);
  if (packet_len) {
    nesclient_notification_buffer[CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION] = packet_len;
    packet_len += CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION;
    // Add '#' to the tail
    nesclient_notification_buffer[packet_len] = CLIENT_SPECIFIC_BUFFER_END_CHARACTER;
    packet_len += 1;
#if NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
    nes_sensor_client_upload_data_and_delay(nesclient_notification_buffer, packet_len);
    //sl_iostream_write(SL_IOSTREAM_STDOUT, nesclient_notification_buffer, packet_len);
#endif // NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE
  }
}
#endif // NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION
