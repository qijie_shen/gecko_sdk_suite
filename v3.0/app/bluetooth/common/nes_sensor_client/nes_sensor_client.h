/***************************************************************************//**
 * @file
 * @brief NES Sensor Client GATT service
 *******************************************************************************
 * # License
 * <b>Copyright </b>
 *******************************************************************************
 *
 *
 ******************************************************************************/

#ifndef NES_SENSOR_CLIENT_H
#define NES_SENSOR_CLIENT_H

#include <stddef.h>
#include <stdint.h>
#include "nes_sensor_client_config.h"

#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT
#ifdef SL_CATALOG_PRINTF_PRESENT
#include "printf.h"
#endif // SL_CATALOG_PRINTF_PRESENT

#if NES_PIR_MEASUREMENT_LOG_ENABLE
#define nes_sensor_client_log(...)                                            printf(__VA_ARGS__)
#else // NES_PIR_MEASUREMENT_LOG_ENABLE
#define nes_sensor_client_log(...)
#endif // NES_PIR_MEASUREMENT_LOG_ENABLE

/// Possible NES sensor data communication modes
/// Can only use macros
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_RAW                       0
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_JSON                      1
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_PROTOBUF                  2

#define NES_CLIENT_NOTIFICATION_BUFFER_LENGTH_BYTES                           200

#define CLIENT_SPECIFIC_HEAD_CHARACTER                                        '%'
#define CLIENT_SPECIFIC_BUFFER_LENGTH_POSITION                                1
#define CLIENT_SPECIFIC_BUFFER_DATA_START_POSITION                            2
#define CLIENT_SPECIFIC_BUFFER_END_CHARACTER                                  '#'

#define NES_SENSOR_CLIENT_RECEIVING_BUFFER_LENGTH_BYTES                       280

#define DATA_EXCHANGE_HEAD_CHARACTER                                          '$'
#define DATA_EXCHANGE_SPLIT_CHARACTER                                         '|'
#define DATA_EXCHANGE_END_CHARACTER                                           '#'

#define DATA_EXCHANGE_NODE_ADDRESS_START_POSITION                             3
#define DATA_EXCHANGE_NODE_ADDRESS_END_POSITION                               7
#define DATA_EXCHANGE_NODE_RSSI_START_POSITION                                8
#define DATA_EXCHANGE_NODE_RSSI_END_POSITION                                  11
#define DATA_EXCHANGE_NODE_BUFFER_LENGTH_POSITION                             12
#define DATA_EXCHANGE_NODE_BUFFER_DATA_START_POSITION                         13
// The fixed number of bytes to send through USART                            // Prefix length + protobuf_length_byte + '#'
#define DATA_EXCHANGE_NODE_FIXED_DATA_LENGTH_BYTES                            (DATA_EXCHANGE_NODE_BUFFER_LENGTH_POSITION + 1 + 1)

/// Possible Watchdog timeout period selection
/// Can only use macros
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_1_SECOND                  1
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND                  2
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_4_SECOND                  4
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_8_SECOND                  8
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND                 16
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_32_SECOND                 32
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_64_SECOND                 64
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_128_SECOND                128
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_256_SECOND                256

#define TICKS_TO_DELAY_ONE_SECOND                                             32768

#if NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS
typedef enum
{
  characteristic_discover_pir_measurement           = 0x0, /**< (0x0) Discover only limited
                                           discoverable devices. */
  characteristic_discover_pir_sleep_measurement     = 0x1, /**< (0x1) Discover limited and generic
                                           discoverable devices. */
  characteristic_discover_done                      = 0x2
} pir_characteristic_discover_mode_t;

#define GATTDB_PIR_MEASUREMENT                                                21
#define GATTDB_PIR_SLEEP_MEASUREMENT                                          24
#endif // NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS

#define NES_PIR_MEASUREMENT_LOGGING_MODE_OFF                                  0
#define NES_PIR_MEASUREMENT_LOGGING_MODE_RAW                                  1
#define NES_PIR_MEASUREMENT_LOGGING_MODE_LIST                                 2

#define NES_PIR_MEASUREMENT_LOGGING_END_CHAR_CR                               '\r'
#define NES_PIR_MEASUREMENT_LOGGING_END_CHAR_LN                               '\n'
/**************************************************************************//**
 * Bluetooth stack event handler.
 * @param[in] evt Event coming from the Bluetooth stack.
 **************************************************************
 *****************************************************************************/
void nes_bt_sensor_client_on_event(sl_bt_msg_t *evt);

#endif // NES_SENSOR_CLIENT_H
