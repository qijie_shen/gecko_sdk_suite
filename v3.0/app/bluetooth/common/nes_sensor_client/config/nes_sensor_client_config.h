/***************************************************************************//**
 * @file
 * @brief NES Sensor Client GATT service configuration
 *******************************************************************************
 * # License
 * <b>Copyright </b>
 *******************************************************************************
 *
 *
 ******************************************************************************/

#ifndef NES_SENSOR_CLIENT_CONFIG_H
#define NES_SENSOR_CLIENT_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> PIR Measurement Configuration

// <h> PIR Measurement Working Status Configuration

// <o NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE> Data communication mode
// <NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_RAW=> Raw
// <NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_JSON=> JSON
// <NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_PROTOBUF=> Protobuf
// <i> Default: NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_RAW
// <i> Define the data communication mode for each indication.
#define NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE                             NES_PIR_MEASUREMENT_DATA_COMMUNICATION_MODE_PROTOBUF

// <q NES_PIR_MEASUREMENT_ASSERT_ENABLE> Enable
// <i> Enables Assert.
#define NES_PIR_MEASUREMENT_ASSERT_ENABLE                                       1

// <q NES_PIR_MEASUREMENT_LOG_ENABLE> Enable
// <i> Enables Log.
#define NES_PIR_MEASUREMENT_LOG_ENABLE                                          0

// <q NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE> Enable
// <i> Enables uploading data.
#define NES_PIR_MEASUREMENT_UPLOAD_DATA_ENABLE                                  1

// <q NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT> Enable
// <i> Enables unpack and print.
#define NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT                             0

// <q NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA> Enable
// <i> Enables unpack and print nes_data.
#define NES_PIR_MEASUREMENT_ENABLE_UNPACK_AND_PRINT_NES_DATA                    0

// <q NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING> Enable the watch dog to avoid running off
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_ONLY_TEMPERATURE_FOR_NETWORK_TESTING         0

// <q NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING> Enable the watch dog to avoid running off
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_NEW_NETWORK_RECORDING                        1

// <q NES_PIR_MEASUREMENT_ENABLE_WATCHDOG> Enable the watch dog to avoid running off
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_WATCHDOG                                     1

// <q NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG> Enable the watch dog running in debug mode
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_WATCHDOG_DEBUG                               0

// <q NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES> Enable the app code to handle network issues or not
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_APP_TO_HANDLE_NETWORK_ISSUES                 0

// <q NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS> Enable the watch dog to avoid running off
// <i> Default: 0
#define NES_PIR_MEASUREMENT_ENABLE_MULTIPLE_INDICATION_CHARACTERISTICS          0

// <o NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS>
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_1_SECOND=> 1_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND=> 2_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_4_SECOND=> 4_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_8_SECOND=> 8_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND=> 16_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_32_SECOND=> 32_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_64_SECOND=> 64_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_128_SECOND=> 128_SECOND
// <NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_256_SECOND=> 256_SECOND
// <i> Default: NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_2_SECOND
// <i> Define the timeout period for watchdog.
#define NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_SECONDS                     NES_PIR_MEASUREMENT_WATCHDOG_TIMEOUT_PERIOD_16_SECOND

// <o NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS> <1-255>
// <i> Default: 1
// <i> Define the period to feed the watch dog, should smaller than the timeout period
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_PERIOD_SECONDS                        8

// <o NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE> <0-255>
// <i> Default: 0
// <i> Define the timer handle to feed the watch dog
#define NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE                          0

// <q NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION> Enable the heart-beat notifications
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_HEART_BEAT_NOTIFICATION                      1

// <o NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_PERIOD_SECONDS> <1-255>
// <i> Default: 1
// <i> Define the period to send heart-beat notifications
#define NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_PERIOD_SECONDS              60

// <o NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_TIMER_HANDLE> <0-255>
// <i> Default: 1
// <i> Define the timer handle to send heart-beat notifications
#define NES_PIR_MEASUREMENT_HEART_BEAT_NOTIFICATION_TIMER_HANDLE                1

// <o NES_PIR_MEASUREMENT_UPLOAD_DATA_THEN_WAIT_US> Enable
// <i> Enables uploading data.
#define NES_PIR_MEASUREMENT_UPLOAD_DATA_THEN_WAIT_US                            0

// <o NES_PIR_MEASUREMENT_LOGGING_MODE> Logging mode
// <NES_PIR_MEASUREMENT_LOGGING_MODE_OFF=> Off
// <NES_PIR_MEASUREMENT_LOGGING_MODE_RAW=> Raw
// <NES_PIR_MEASUREMENT_LOGGING_MODE_LIST=> List
#define NES_PIR_MEASUREMENT_LOGGING_MODE                                        NES_PIR_MEASUREMENT_LOGGING_MODE_RAW

// <o NES_PIR_MEASUREMENT_LOGGING_END_CHAR> Logging end char
// <NES_PIR_MEASUREMENT_LOGGING_END_CHAR_CR=> CR
// <NES_PIR_MEASUREMENT_LOGGING_END_CHAR_LN=> LN
#define NES_PIR_MEASUREMENT_LOGGING_END_CHAR                                    NES_PIR_MEASUREMENT_LOGGING_END_CHAR_CR

// <q NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION> Enable the heart-beat notifications
// <i> Default: 1
#define NES_PIR_MEASUREMENT_ENABLE_GPIO_INDICATION                              1

// <o NES_PIR_MEASUREMENT_GPIO_INDICATION_PERIOD_TICKS> <1-255>
// <i> Default: 1
// <i> Define the period to feed the watch dog, should smaller than the timeout period
#define NES_PIR_MEASUREMENT_GPIO_INDICATION_PERIOD_SECONDS                      2

// <o NES_PIR_MEASUREMENT_WATCHDOG_FEED_TIMER_HANDLE> <0-255>
// <i> Default: 0
// <i> Define the timer handle to feed the watch dog
#define NES_PIR_MEASUREMENT_GPIO_INDICATION_TIMER_HANDLE                        2

// <q NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1> Use push button1 as the GPIO detection input for debugging
// <i> Default: 1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1               0

// </h> End PIR Measurement Working Status Configuration

// </h> End PIR Measurement Configuration

// <<< end of configuration section >>>

// <<< sl:start pin_tool >>>

// GPIO Detection GPIOs
// <gpio> NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION - P1 on WSTK
// $[GPIO_NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION]
#if NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT               gpioPortF
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN                7
#else // !NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PORT               gpioPortC
#define NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_PIN                6
#endif // NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION_USE_PUSH_BUTTON1
// [GPIO_NES_PIR_MEASUREMENT_GPIO_LEVEL_DETECTION]$

// <<< sl:end pin_tool >>>

#endif // NES_SENSOR_CLIENT_CONFIG_H
