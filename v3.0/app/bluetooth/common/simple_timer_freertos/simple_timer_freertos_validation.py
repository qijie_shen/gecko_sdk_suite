# simple_timer_freertos validation script for configUSE_TIMERS

def validate(project):
  timer_enable = project.config('configUSE_TIMERS')
  if timer_enable is not None:
    if int(timer_enable.value()) == 0:
      project.error('Kernel configUSE_TIMERS config needs to be enabled', project.target_for_defines('configUSE_TIMERS'))
