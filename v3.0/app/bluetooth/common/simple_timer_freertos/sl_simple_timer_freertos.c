/***************************************************************************//**
 * @file
 * @brief Simple timer service based on FreeRTOS
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include <stdbool.h>
#include "freertos_sl_system.h"
#include "sl_simple_timer.h"

// -----------------------------------------------------------------------------
// Private function declarations

/***************************************************************************//**
 * Check if the timer has already been created.
 *
 * @param timer[in] timer handle
 * @return true if timer has already been created, false otherwise.
 ******************************************************************************/
static bool timer_is_created(sl_simple_timer_t * timer);

// -----------------------------------------------------------------------------
// Public function definitions

sl_status_t sl_simple_timer_start(sl_simple_timer_t *timer,
                                  uint32_t timeout_ms,
                                  sl_simple_timer_callback_t callback,
                                  void *callback_data,
                                  bool is_periodic)
{
  (void)callback_data;
  BaseType_t err;
  TickType_t tick_rate;
  TickType_t delay, period;
  UBaseType_t opt;

  // Check input parameters.
  if ( (timeout_ms == 0) && is_periodic ) {
    return SL_STATUS_INVALID_PARAMETER;
  }
  if ( timer == NULL ) {
    return SL_STATUS_INVALID_PARAMETER;
  }

  // Calculate timer period.
  tick_rate = configTICK_RATE_HZ;
  delay = (timeout_ms * tick_rate) / (1000);
  if ( delay == 0 ) {
    // The timer resolution is too small for the requested timeout.
    return SL_STATUS_INVALID_PARAMETER;
  }
  period = delay; //is_periodic ? delay : 0;
  opt = is_periodic ? pdTRUE : pdFALSE;

  // Make sure that timer is stopped, also check for NULL.
  sl_simple_timer_stop(*timer);

  // Create OS timer.
  *timer = xTimerCreate("simple timer",
                        period,
                        opt,
                        (void*)0,
                        (TimerCallbackFunction_t)callback
                        );

  if ( (timer == NULL) || (*timer == NULL) ) {
    return SL_STATUS_ALLOCATION_FAILED;
  }

  // Start OS timer.
  err = xTimerStart(*timer, 0);
  if ( err != pdPASS ) {
    sl_simple_timer_stop(*timer);
    return SL_STATUS_FAIL;
  }
  return SL_STATUS_OK;
}

sl_status_t sl_simple_timer_stop(sl_simple_timer_t *timer)
{
  BaseType_t err;

  if ( timer_is_created(timer) ) {
    // Stop and delete OS timer.
    err = xTimerDelete(*timer, 100);
    if ( err != pdPASS ) {
      return SL_STATUS_INVALID_PARAMETER;
    }
    *timer = NULL;
  }

  return SL_STATUS_OK;
}

// -----------------------------------------------------------------------------
// Private function definitions

static bool timer_is_created(sl_simple_timer_t * timer)
{
  bool ret = false;
  if ( timer != NULL && *timer != NULL ) {
    // If has a name then created
    if ( pcTimerGetName(*timer) != NULL ) {
      ret = true;
    }
  }
  return ret;
}
