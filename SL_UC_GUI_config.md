# Configure Part
## Start : // <<< Use Configuration Wizard in Context Menu >>>
### Configuration Block - no checkbox (nested)
#### Start: // <h> {Block Description}
#### End : // </h> {End Block Description}

### Configuration Block - with checkbox {nested}
#### Start: // <e> {Block Description}
#### End : // </e>
#### Note: <h> and <e> have the same level

### Enable Switch
#### Start: // <q {Define Name}> {Abbreviation}
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} {value: 0|1}

### ComboBox
#### Start: // <o {Define Name}> {Abbreviation}
#### Middle: // <EnumValue-1=> EnumValue1 Description
#### Middle: // <EnumValue-2=> EnumValue2 Description
#### Middle: // <EnumValue-3=> EnumValue3 Description
#### Middle: // <EnumValue-4=> EnumValue4 Description
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} EnumValue-N
#### Note: This define can be used to preprocessor switch or enum switch
#### Note: Preprocessor switch can only use macros

### SpinBox (Editedable)
#### Start: // <o {Define Name}> {Abbreviation} <MinValue-MaxValue> (range is optional)
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} Number

### LineEdit1 - without length limit 
#### Start: // <s {Define Name}> {Abbreviation}
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} "String"

### LineEdit2 - with length limit 
#### Start: // <s.n {Define Name}> {Abbreviation}
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} "String" (The length of the string is limited by n) (Length can be 0)

### Single Bit CheckBox
#### Start: // <o.n> {Abbreviation}
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} Number (Modify the specified bit(0/1) of the default number)

### Bit Range SpinBox
#### Start: // <o.x..y> {Abbreviation} <MinValue-MaxValue> (range is optional) (range can be decimal or hex)
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name} Number (Decimal or Hex mode)
#### Note0: The given number matters. The configured value is the bit switch of the given number.
#### Note1: The modification step is 2^x.

### Selective SpinBox (Editedable)
#### Start: // <oN> {Abbreviation} <MinValue-MaxValue> (range is optional)
#### Middle: // <i> {Some information: Default, Description...}
#### End: #define {Define Name1} Number1
#### End: #define {Define Name2} Number2
#### End: #define {Define Name3} Number3
#### Note: The spinbox can only modify the item specified by N.

## End : // <<< end of configuration section >>>

Modifier	 				Description
<0-31>				no		Value range for option fields.
<0-100:10>			no		Value range for option fields with step 10.
<0x40-0x1000:0x10>	no		Value range in hex format and step 10.
<value=>			yes		Creates a drop down-list and displays the text. value is written to the next item. 
<identifier=>		yes		Creates a drop down-list and displays the text following the definition of the identifiers dwt, systick and user. Note that this must only be used in the context of <o key-identifier>! The identifier corresponding to the selected text replaces the identifier following the key-identifier specified by the <o ...> tag.
<#+1>   
<#-1>
<#*8>   
<#/3>				no		Modifies the entered or displayed value according to the operator (add, sub, mul, div). The changed value is set for the code symbol.

# PinTool Part